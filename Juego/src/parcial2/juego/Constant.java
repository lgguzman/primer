package parcial2.juego;

public interface Constant {
	 static final short FLAG_MESSAGE_SERVER_ADD_FACE = 1;
	 static final short FLAG_MESSAGE_SERVER_MOVE_CARD = FLAG_MESSAGE_SERVER_ADD_FACE + 1;
	 static final short FLAG_MESSAGE_CLIENT_ADD_FACE = FLAG_MESSAGE_SERVER_MOVE_CARD+1 ;
	 static final short FLAG_MESSAGE_CLIENT_MOVE_CARD = FLAG_MESSAGE_CLIENT_ADD_FACE + 1;
	 static final short FLAG_MESSAGE_CLIENT_ID_USER = FLAG_MESSAGE_CLIENT_MOVE_CARD + 1;
	 static final short FLAG_MESSAGE_SERVER_ID_USER = FLAG_MESSAGE_CLIENT_ID_USER + 1;
	 static final short FLAG_MESSAGE_SERVER_LISTO = FLAG_MESSAGE_SERVER_ID_USER + 1;
	 static final short FLAG_MESSAGE_SERVER_MDESTROY = FLAG_MESSAGE_SERVER_LISTO + 1;
	 static final short FLAG_MESSAGE_SERVER_GFINISH = FLAG_MESSAGE_SERVER_MDESTROY + 1;
	 static final short FLAG_MESSAGE_CLIENT_GFINISH = FLAG_MESSAGE_SERVER_GFINISH + 1;
	 static final int ncards=8;
	 static final int ccards=4;
	 static final int wcard=80;
	 static final int hcard=111;
	 static final int LAYER_BACKGROUND=0;
	 static final String LOCALHOST_IP = "127.0.0.1";
     static final int CAMERA_WIDTH = 640;
	 static final int CAMERA_HEIGHT = 480;
}
