package parcial2.juego;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import org.andengine.engine.Engine;
import org.andengine.engine.Engine.EngineLock;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;

import org.andengine.entity.scene.IOnAreaTouchListener;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.util.FPSLogger;
import org.andengine.examples.adt.messages.client.ClientMessageFlags;
import org.andengine.examples.adt.messages.server.ConnectionCloseServerMessage;
import org.andengine.examples.adt.messages.server.ServerMessageFlags;
import org.andengine.extension.multiplayer.protocol.adt.message.IMessage;

import org.andengine.extension.multiplayer.protocol.adt.message.client.IClientMessage;
import org.andengine.extension.multiplayer.protocol.adt.message.server.IServerMessage;

import org.andengine.extension.multiplayer.protocol.client.IServerMessageHandler;
import org.andengine.extension.multiplayer.protocol.client.connector.ServerConnector;
import org.andengine.extension.multiplayer.protocol.client.connector.SocketConnectionServerConnector;
import org.andengine.extension.multiplayer.protocol.client.connector.SocketConnectionServerConnector.ISocketConnectionServerConnectorListener;
import org.andengine.extension.multiplayer.protocol.server.IClientMessageHandler;
import org.andengine.extension.multiplayer.protocol.server.SocketServer;
import org.andengine.extension.multiplayer.protocol.server.SocketServer.ISocketServerListener;
import org.andengine.extension.multiplayer.protocol.server.connector.ClientConnector;
import org.andengine.extension.multiplayer.protocol.server.connector.SocketConnectionClientConnector;
import org.andengine.extension.multiplayer.protocol.server.connector.SocketConnectionClientConnector.ISocketConnectionClientConnectorListener;
import org.andengine.extension.multiplayer.protocol.shared.SocketConnection;
import org.andengine.extension.multiplayer.protocol.util.MessagePool;
import org.andengine.extension.multiplayer.protocol.util.WifiUtils;
import org.andengine.input.touch.TouchEvent;

import org.andengine.ui.activity.SimpleBaseGameActivity;
import org.andengine.util.debug.Debug;

import parcial2.juego.Messenger.*;
import parcial2.juego.SpriteManager.CardUserData;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.Toast;

/**
 * (c) 2010 Nicolas Gramlich (c) 2011 Zynga
 * 
 * @author Nicolas Gramlich
 * @since 17:10:24 - 19.06.2010
 */
public class Batalla extends SimpleBaseGameActivity implements
		ClientMessageFlags, ServerMessageFlags, Constant {
	// ===========================================================
	// Constants
	// ===========================================================


	public boolean empezar = false;
	public boolean empezado = false;
	private static final int SERVER_PORT = 4444;
	private boolean isactive;
	private int nplayers = 0;
	private boolean listo;
	public int vida = 5;
	public int vidaEnemigo = 5;
	public int cartaEnemigo;
	public boolean semaforomessage = true;
	static int batallaid;
	public HttpHandler h = HttpHandler.getInstance();
	public int messageID = -1;
	public int lastMessage = -1;
	public int countMessage = 3;
	public int clientmessageID =-1;
	public int lastClientMessage1=-1;
	public int lastClientMessage2=-1;
	public boolean imserver=false;
	public short countini=0;

	private static final int DIALOG_CHOOSE_SERVER_OR_CLIENT_ID = 0;
	private static final int DIALOG_ENTER_SERVER_IP_ID = DIALOG_CHOOSE_SERVER_OR_CLIENT_ID + 1;
	private static final int DIALOG_SHOW_SERVER_IP_ID = DIALOG_ENTER_SERVER_IP_ID + 1;
	private static final int DIALOG_OK = DIALOG_SHOW_SERVER_IP_ID  + 1;
	// ===========================================================
	// Fields
	// ===========================================================
	// ===========================================================
	// TEXTURES
	// ==========================================================
	private SpriteManager spritemanager = new SpriteManager(this);
	private UsuarioClass usuario = UserSession.getInstance().usuario;
	private int usuarioid = Integer.parseInt(usuario.id);
	private int turno;
	private String mServerIP = LOCALHOST_IP;
	private SocketServer<SocketConnectionClientConnector> mSocketServer;
	private ServerConnector<SocketConnection> mServerConnector;
	private GameLogic gameLogic = new GameLogic();

	private final MessagePool<IMessage> mMessagePool = new MessagePool<IMessage>();

	// ===========================================================
	// Constructors
	// ===========================================================

	public Batalla() {
		this.initMessagePool();
		isactive = true;
	}

	private void initMessagePool() {
		this.mMessagePool.registerMessage(FLAG_MESSAGE_SERVER_ADD_FACE,
				AddFaceServerMessage.class);
		this.mMessagePool.registerMessage(FLAG_MESSAGE_SERVER_MOVE_CARD,
				MoveCardServerMessage.class);
		this.mMessagePool.registerMessage(FLAG_MESSAGE_CLIENT_ADD_FACE,
				AddFaceClientMessage.class);
		this.mMessagePool.registerMessage(FLAG_MESSAGE_CLIENT_MOVE_CARD,
				MoveCardClientMessage.class);
		this.mMessagePool.registerMessage(FLAG_MESSAGE_CLIENT_ID_USER,
				UserClientMessage.class);
		this.mMessagePool.registerMessage(FLAG_MESSAGE_SERVER_ID_USER,
				UserServerMessage.class);
		this.mMessagePool.registerMessage(FLAG_MESSAGE_SERVER_LISTO,
				ListoServerMessage.class);
		this.mMessagePool.registerMessage(FLAG_MESSAGE_SERVER_MDESTROY,
				MDestroyServerMessage.class);
		this.mMessagePool.registerMessage(FLAG_MESSAGE_SERVER_GFINISH,
				GFinishServerMessage.class);
		this.mMessagePool.registerMessage(FLAG_MESSAGE_CLIENT_GFINISH,
				GFinishClientMessage.class);

	}

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	@SuppressWarnings("deprecation")
	@Override
	public EngineOptions onCreateEngineOptions() {
		
		
		final Camera camera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
		final EngineOptions engineOptions =  new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED,
				new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), camera);
		engineOptions.getAudioOptions().setNeedsMusic(true);
		return engineOptions;
	}

	@Override
	public void onCreateResources() {

		spritemanager.chargeTexture();

	}

	public int[] inicard() {
		int card[] = new int[5];

		for (int i = 0; i < 5; i++) {
			card[i] = Integer.parseInt(usuario.cartas.get(i).imagen);
		}
		return card;
	}

	@Override
	public Scene onCreateScene() {
		this.mEngine.registerUpdateHandler(new FPSLogger());
	//	this.showDialog(DIALOG_OK);
		usuario.hasCardActive = false;
		
		final Scene scene = new Scene();
        
		try{
		mServerIP= WifiUtils.getWifiIPv4Address(this);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				 try{       if(countini==0)
					 {countini++;
						String s=h.ipServer(mServerIP, usuario.enemy, usuario.id);
						Thread.sleep(1000); 
						s=s.substring(s.indexOf(";")+1, s.length());
						 s=s.substring(0, s.indexOf(";"));
						 if(s.indexOf("noC")<0){
							 mServerIP=s;
							 Batalla.this.initClient();
							
						 }
						 else
						 {   mServerIP=LOCALHOST_IP;
							 Batalla.this.initServerAndClient();
							 
						 }}
				         }catch(Exception e){
				        	 e.printStackTrace();
				        	 Batalla.this.finish();
				         }
				
			}
		}).start();
		
		
		
		
		
		int card1[] = inicard();
		spritemanager.loadBackground(scene, card1, (usuarioid), usuario.cartas);
		Thread thr = new Thread(new Runnable() {
			@Override
			public void run() {
			
				long l = System.currentTimeMillis();
				while ((!empezado && Batalla.this.mSocketServer == null)
						|| (Batalla.this.mSocketServer != null && (!empezado || nplayers < 2))) {

					long time = System.currentTimeMillis() - l;
					if (time == 50000) {
						toast("Ningun cliente conectado");
						Batalla.this.finish();
					}
					if (!isactive) {

						Batalla.this.finish();
						break;
					}

				}
				if (isactive) {
					
					if (turno == (usuarioid))
						spritemanager.cadenatiempop("Tu turno!", 100, 100);
					spritemanager.sound();
					// if(Batalla.this.mSocketServer != null) {
					/*
					 * scene.setOnSceneTouchListener(new IOnSceneTouchListener()
					 * {
					 * 
					 * @Override public boolean onSceneTouchEvent(final Scene
					 * pScene, final TouchEvent pSceneTouchEvent) {
					 * if(pSceneTouchEvent.isActionDown()) { try {
					 * 
					 * } catch (final IOException e) { Debug.e(e); } return
					 * true; } else { return true; } } });
					 */

					scene.setOnAreaTouchListener(new IOnAreaTouchListener() {
						@Override
						public boolean onAreaTouched(
								final TouchEvent pSceneTouchEvent,
								final ITouchArea pTouchArea,
								final float pTouchAreaLocalX,
								final float pTouchAreaLocalY) {
							 if(pSceneTouchEvent.isActionUp()){
							try {
								
								if (nplayers > 1
										|| Batalla.this.mSocketServer == null) {
									final Sprite card = (Sprite) pTouchArea;
									final CardUserData userData = (CardUserData) card
											.getUserData();

									if (userData.ownerID == (usuarioid)) {

										
										if (!userData.active
												&& !usuario.hasCardActive
												&& semaforomessage) {
											semaforomessage = false;
											toastl("Moviendo, por favor espere...");
											MoveCardClientMessage MoveCardClientMessage = (MoveCardClientMessage) Batalla.this.mMessagePool
													.obtainMessage(FLAG_MESSAGE_CLIENT_MOVE_CARD);
											((CardUserData) card.getUserData()).active = true;
											Thread.sleep(200);
											int pos = ((CardUserData) card
													.getUserData()).cardID;
											int sprite = ((CardUserData) card
													.getUserData()).spritecard;
											int live = usuario.cartas.get(pos).vida;
											int power = usuario.cartas.get(pos).poder;
											int defend = usuario.cartas
													.get(pos).defenza;
											int attack = usuario.cartas
													.get(pos).ataque;
                                            for(int i=0;i<countMessage;i++){ 
                                            	Thread.sleep(100);
                                            	clientmessageID++;
												MoveCardClientMessage
														.set(userData.GraphicID,
																pSceneTouchEvent
																		.getX(),
																pSceneTouchEvent
																		.getY(),
																userData.ownerID,
																live,
																power,
																defend,
																attack,
																pos,
																userData.active,
																sprite,
																clientmessageID);
												Batalla.this.mServerConnector
														.sendClientMessage(MoveCardClientMessage);
                                            }
                                           // toast("envio move");
											Batalla.this.mMessagePool
													.recycleMessage(MoveCardClientMessage);
										} else {
											
                                        /*      if(turno==usuarioid)
                                            	  toast("turno");
                                              if(listo)
                                            	  toast("listo");
                                              if(usuario.hasCardActive)
                                            	  toast("UsrHasCard");
                                              if(semaforomessage)
                                            	  toast("semaforoMesa");*/
                                            		  
                                               
												if (userData.active
														&& turno == (usuarioid)
														&& listo
														&& usuario.hasCardActive
														&& semaforomessage) {
													semaforomessage = false;
													toastl("Atacando, por favor espere...");
													AddFaceClientMessage addFaceClientMessage = (AddFaceClientMessage) Batalla.this.mMessagePool
															.obtainMessage(FLAG_MESSAGE_CLIENT_ADD_FACE);
													for(int i=0;i<countMessage;i++){ 
		                                            	clientmessageID++;
														addFaceClientMessage
																.set(usuarioid,
																		false,
																		turno, clientmessageID);
														Batalla.this.mServerConnector
																.sendClientMessage(addFaceClientMessage);
													}
													//toast("envio attack");
													Batalla.this.mMessagePool
															.recycleMessage(addFaceClientMessage);
												}

										}
									}

								}
							} catch (final Exception e) {
								e.printStackTrace();
								return false;
							}}
							return true;
						}
					});

					scene.setTouchAreaBindingOnActionDownEnabled(true);
					// }
				}
			}
		});
		thr.start();

		return scene;
	}

	public Engine getEngine() {

		return this.mEngine;
	}

	// ===========================================================================================
	// MENSAJE INICIAL
	// =======================================================================================

	@Override
	protected Dialog onCreateDialog(final int pID) {

		switch (pID) {
		case DIALOG_SHOW_SERVER_IP_ID: {
			try {
				return new AlertDialog.Builder(this)
						.setIcon(android.R.drawable.ic_dialog_info)
						.setTitle("IP del servidor")
						.setCancelable(false)
						.setMessage(
								"La ip de tu servidor es:\n"
										+ WifiUtils.getWifiIPv4Address(this))
						.setPositiveButton(android.R.string.ok, null).create();
			} catch (final UnknownHostException e) {
				return new AlertDialog.Builder(this)
						.setIcon(android.R.drawable.ic_dialog_alert)
						.setTitle("IP del servidor")
						.setCancelable(false)
						.setMessage("error al buscar ip del servidor" + e)
						.setPositiveButton(android.R.string.ok,
								new OnClickListener() {
									@Override
									public void onClick(
											final DialogInterface pDialog,
											final int pWhich) {
										Batalla.this.finish();
									}
								}).create();

			}

		}
		case DIALOG_ENTER_SERVER_IP_ID:
			final EditText ipEditText = new EditText(this);
			return new AlertDialog.Builder(this)
					.setIcon(android.R.drawable.ic_dialog_info)
					.setTitle("Ingresa la IP del Servidor")
					.setCancelable(false).setView(ipEditText)
					.setPositiveButton("Conectar", new OnClickListener() {
						@Override
						public void onClick(final DialogInterface pDialog,
								final int pWhich) {
							Batalla.this.mServerIP = ipEditText.getText()
									.toString();
							Batalla.this.initClient();
						}
					}).setNegativeButton("Cancelar", new OnClickListener() {
						@Override
						public void onClick(final DialogInterface pDialog,
								final int pWhich) {
							empezar = true;
							Batalla.this.finish();
						}
					}).create();
		case DIALOG_CHOOSE_SERVER_OR_CLIENT_ID:
			if (!empezar) {

				empezar = true;
				Dialog dialog = new AlertDialog.Builder(this)
						.setIcon(android.R.drawable.ic_dialog_info)
						.setTitle("Ser Cliente o servidor")
						.setCancelable(false)
						.setPositiveButton("Cliente", new OnClickListener() {
							@Override
							public void onClick(final DialogInterface pDialog,
									final int pWhich) {
								Batalla.this
										.showDialog(DIALOG_ENTER_SERVER_IP_ID);
								// Batalla.this.mServerIP = "192.168.1.5";
								// Batalla.this.initClient();
							}
						}).setNegativeButton("Servidor", new OnClickListener() {
							@Override
							public void onClick(final DialogInterface pDialog,
									final int pWhich) {
								Batalla.this.initServerAndClient();
								Batalla.this
										.showDialog(DIALOG_SHOW_SERVER_IP_ID);
							}
						}).create();

				return dialog;

			} else
				return new AlertDialog.Builder(this)
						.setIcon(android.R.drawable.ic_dialog_info)
						.setTitle("OK").setCancelable(false).setMessage("OK")
						.setPositiveButton(android.R.string.ok, null).create();
		case DIALOG_OK:
			return new AlertDialog.Builder(this)
			.setIcon(android.R.drawable.ic_dialog_info)
			.setTitle("OK").setCancelable(false).setMessage("OK")
			.setPositiveButton(android.R.string.ok, null).create();
		default:
			return super.onCreateDialog(pID);
		}
	}

	@Override
	protected void onDestroy() {
		try{
			spritemanager.mMusic.stop();
		h.activoOff(usuarioid+"");}
		catch(Exception r){
			r.printStackTrace();
		}
		if (this.mSocketServer != null) {
			try {
				this.mSocketServer
						.sendBroadcastServerMessage(new ConnectionCloseServerMessage());
			} catch (final IOException e) {
				Debug.e(e);
			}
			this.mSocketServer.terminate();
		}

		if (this.mServerConnector != null) {
			this.mServerConnector.terminate();
		}
		isactive = false;
		super.onDestroy();
	}

	@Override
	public boolean onKeyUp(final int pKeyCode, final KeyEvent pEvent) {
		switch (pKeyCode) {
		case KeyEvent.KEYCODE_BACK:
			this.finish();
			return true;
		}
		return super.onKeyUp(pKeyCode, pEvent);
	}

	// ===========================================================
	// Methods
	// ===========================================================

	// ===================================
	// user dATA
	// =====================================

	// =======================================================================================================
	// CLIENTE Y SEVIDOR.... INICIALIZACION
	// =======================================================================================================

	private void initServerAndClient() {
		this.initServer();

		/*
		 * Wait some time after the server has been started, so it actually can
		 * start up.
		 */

		this.initClient();
	}

	private void initServer() {
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				mSocketServer = new SocketServer<SocketConnectionClientConnector>(
						SERVER_PORT, new ExampleClientConnectorListener(),
						new ExampleServerStateListener()) {
					@Override
					protected SocketConnectionClientConnector newClientConnector(
							final SocketConnection pSocketConnection)
							throws IOException {
						empezado = true;
						final SocketConnectionClientConnector clientConector = new SocketConnectionClientConnector(
								pSocketConnection);

						clientConector.registerClientMessage(
								FLAG_MESSAGE_CLIENT_ADD_FACE,
								AddFaceClientMessage.class,
								new IClientMessageHandler<SocketConnection>() {
									@Override
									public void onHandleMessage(
											final ClientConnector<SocketConnection> pClientConnector,
											final IClientMessage pClientMessage)
											throws IOException {
										try {
											final AddFaceClientMessage addFaceClientMessage = (AddFaceClientMessage) pClientMessage;
	                                        
											if ((addFaceClientMessage.messageID
													/ countMessage > lastClientMessage1 
													&& addFaceClientMessage.owner==usuarioid) || 
													(addFaceClientMessage.messageID
															/ countMessage > lastClientMessage2 
															&& addFaceClientMessage.owner!=usuarioid)
													) {
												
											    if(addFaceClientMessage.owner==usuarioid)
												{lastClientMessage1 = addFaceClientMessage.messageID
														/ countMessage;
												//toast("cliente 1 tiene"+lastClientMessage1+" y el mensaje fue "+addFaceClientMessage.messageID);
												}
											    else
											    	{lastClientMessage2 = addFaceClientMessage.messageID
													/ countMessage;
											    	//toast("cliente 2 tiene"+lastClientMessage2+" y el mensaje fue "+addFaceClientMessage.messageID);
											    	}
											
											int tempturn = (usuarioid);
											if (addFaceClientMessage.turn == tempturn)
												tempturn = Integer
														.parseInt(usuario.enemy);

	 										if (!gameLogic.attack(
													addFaceClientMessage.owner,
													addFaceClientMessage.power)) {

												final AddFaceServerMessage addFaceServerMessage = (AddFaceServerMessage) Batalla.this.mMessagePool
														.obtainMessage(FLAG_MESSAGE_SERVER_ADD_FACE);
												for (int i = 0; i < countMessage; i++) {
													messageID++;
													addFaceServerMessage
															.set(gameLogic.user2.mID,
																	gameLogic.user2.userID,
																	gameLogic.user2.live,
																	tempturn,
																	messageID);

													Batalla.this.mSocketServer
															.sendBroadcastServerMessage(addFaceServerMessage);
												}
											//	toast("servidor ataco no destuyo");
												Batalla.this.mMessagePool
														.recycleMessage(addFaceServerMessage);
											} else {
												final MDestroyServerMessage addFaceServerMessage = (MDestroyServerMessage) Batalla.this.mMessagePool
														.obtainMessage(FLAG_MESSAGE_SERVER_MDESTROY);
												for (int i = 0; i < countMessage; i++) {
													messageID++;
													addFaceServerMessage
															.set(gameLogic.user2.mID,
																	gameLogic.user2.userID,
																	gameLogic.user2.live,
																	tempturn,
																	gameLogic.user1.mID,
																	messageID);

													Batalla.this.mSocketServer
															.sendBroadcastServerMessage(addFaceServerMessage);
												}
											//	toast("servidor destruyo");
												Batalla.this.mMessagePool
														.recycleMessage(addFaceServerMessage);
												final ListoServerMessage listoServerMessage = (ListoServerMessage) Batalla.this.mMessagePool
														.obtainMessage(FLAG_MESSAGE_SERVER_LISTO);
												for (int i = 0; i < countMessage; i++) {
													messageID++;
													listoServerMessage.set(
															false, messageID);
													Batalla.this.mSocketServer
															.sendBroadcastServerMessage(listoServerMessage);
												}
												//toast("servidor envio listo");
												Batalla.this.mMessagePool
														.recycleMessage(listoServerMessage);
											}

											}
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
								});

						clientConector.registerClientMessage(
								FLAG_MESSAGE_CLIENT_MOVE_CARD,
								MoveCardClientMessage.class,
								new IClientMessageHandler<SocketConnection>() {
									@Override
									public void onHandleMessage(
											final ClientConnector<SocketConnection> pClientConnector,
											final IClientMessage pClientMessage)
											throws IOException {
										try {
											final MoveCardClientMessage moveCardClientMessage = (MoveCardClientMessage) pClientMessage;
											
											if ((moveCardClientMessage.messageID
													/ countMessage > lastClientMessage1 
													&& moveCardClientMessage.userID==usuarioid) || 
													(moveCardClientMessage.messageID
															/ countMessage > lastClientMessage2 
															&& moveCardClientMessage.userID!=usuarioid)
													) {
												
											    if(moveCardClientMessage.userID==usuarioid)
												{lastClientMessage1 = moveCardClientMessage.messageID
														/ countMessage;
											//	toast("cliente 1 tiene"+lastClientMessage1+" y el mensaje fue "+moveCardClientMessage.messageID);
												}
											    else
											    	{lastClientMessage2 = moveCardClientMessage.messageID
													/ countMessage; 
											    //	toast("cliente 2 tiene"+lastClientMessage2+" y el mensaje fue "+moveCardClientMessage.messageID);
											    	}
											
											MoveCardServerMessage moveCardServerMessage = (MoveCardServerMessage) Batalla.this.mMessagePool
													.obtainMessage(FLAG_MESSAGE_SERVER_MOVE_CARD);

											if (gameLogic
													.updateGraphicCard(
															moveCardClientMessage.mID,
															moveCardClientMessage.mX,
															moveCardClientMessage.mY,
															moveCardClientMessage.userID,
															moveCardClientMessage.live,
															moveCardClientMessage.power,
															moveCardClientMessage.defend,
															moveCardClientMessage.attack,
															moveCardClientMessage.cardID,
															moveCardClientMessage.active)) {
												final ListoServerMessage listoServerMessage = (ListoServerMessage) Batalla.this.mMessagePool
														.obtainMessage(FLAG_MESSAGE_SERVER_LISTO);
												 Thread.sleep(800);
												for (int i = 0; i < countMessage; i++) {
                                                      
													messageID++;

													listoServerMessage.set(
															true, messageID);
													Batalla.this.mSocketServer
															.sendBroadcastServerMessage(listoServerMessage);
												}
												//toast("servidor envio listo true");
												Batalla.this.mMessagePool
														.recycleMessage(listoServerMessage);
											}
											// (final int pID, final float pX,
											// final float pY, final int
											// userID,final int live,final int
											// power,final int defend, final int
											// attack, int cardID, boolean
											// active)

											for (int i = 0; i < countMessage; i++) {
												messageID++;

												moveCardServerMessage
														.set(moveCardClientMessage.mID,
																moveCardClientMessage.mX,
																moveCardClientMessage.mY,
																moveCardClientMessage.userID,
																moveCardClientMessage.live,
																moveCardClientMessage.power,
																moveCardClientMessage.defend,
																moveCardClientMessage.attack,
																moveCardClientMessage.cardID,
																moveCardClientMessage.active,
																moveCardClientMessage.spritecard,
																messageID);
												Batalla.this.mSocketServer
														.sendBroadcastServerMessage(moveCardServerMessage);

											}
                                                       //toast("servidor envio mover");
											Batalla.this.mMessagePool
													.recycleMessage(moveCardServerMessage);
											}
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
								});

						clientConector.registerClientMessage(
								FLAG_MESSAGE_CLIENT_ID_USER,
								UserClientMessage.class,
								new IClientMessageHandler<SocketConnection>() {
									@Override
									public void onHandleMessage(
											final ClientConnector<SocketConnection> pClientConnector,
											final IClientMessage pClientMessage)
											throws IOException {
										try {
											final UserClientMessage userClientMessage = (UserClientMessage) pClientMessage;
								               
											if ((userClientMessage.messageID
													/ countMessage > lastClientMessage1 
													&& userClientMessage.mID==usuarioid) || 
													(userClientMessage.messageID
															/ countMessage > lastClientMessage2 
															&& userClientMessage.mID!=usuarioid)
													) {
												
											    if(userClientMessage.mID==usuarioid)
												{lastClientMessage1 = userClientMessage.messageID
														/ countMessage;
											//	toast("cliente 1 tiene"+lastClientMessage1+" y el mensaje fue "+userClientMessage.messageID);
												}
											    else
											    	{lastClientMessage2 = userClientMessage.messageID
													/ countMessage; 
											    //	toast("cliente 2 tiene"+lastClientMessage2+" y el mensaje fue "+userClientMessage.messageID);
											    	}
											
											final UserServerMessage userServerMessage = (UserServerMessage) Batalla.this.mMessagePool
													.obtainMessage(FLAG_MESSAGE_SERVER_ID_USER);
											for (int i = 0; i < countMessage; i++) {
												messageID++;
												userServerMessage.set(
														(usuarioid),
														Integer.parseInt(usuario.enemy),
														messageID);

												Batalla.this.mSocketServer
														.sendBroadcastServerMessage(userServerMessage);
											}
											Batalla.this.mMessagePool
													.recycleMessage(userServerMessage);
											}
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
								});

						clientConector.registerClientMessage(
								FLAG_MESSAGE_CLIENT_GFINISH,
								GFinishClientMessage.class,
								new IClientMessageHandler<SocketConnection>() {
									@Override
									public void onHandleMessage(
											final ClientConnector<SocketConnection> pClientConnector,
											final IClientMessage pClientMessage)
											throws IOException {
										try {
											final GFinishClientMessage gFinishClientMessage = (GFinishClientMessage) pClientMessage;
									
											if ((gFinishClientMessage.messageID
													/ countMessage > lastClientMessage1 
													&& gFinishClientMessage.mID==usuarioid) || 
													(gFinishClientMessage.messageID
															/ countMessage > lastClientMessage2 
															&& gFinishClientMessage.mID!=usuarioid)
													) {
												
											    if(gFinishClientMessage.mID==usuarioid)
												{lastClientMessage1 = gFinishClientMessage.messageID
														/ countMessage;
											//	toast("cliente 1 tiene"+lastClientMessage1+" y el mensaje fue "+gFinishClientMessage.messageID);
												}
											    else
											    	{lastClientMessage2 =gFinishClientMessage.messageID
													/ countMessage; 
											 //   	toast("cliente 2 tiene"+lastClientMessage2+" y el mensaje fue "+gFinishClientMessage.messageID);
											    	}
											    
											final GFinishServerMessage gFinishServerMessage = (GFinishServerMessage) Batalla.this.mMessagePool
													.obtainMessage(FLAG_MESSAGE_SERVER_GFINISH);
											for (int i = 0; i < countMessage; i++) {
												messageID++;
												gFinishServerMessage
														.set(gFinishClientMessage.mID,
																gFinishClientMessage.enemy,
																messageID);

												Batalla.this.mSocketServer
														.sendBroadcastServerMessage(gFinishServerMessage);
											}
											//toast("servidor envio finalizar");
											Batalla.this.mMessagePool
													.recycleMessage(gFinishServerMessage);
											}
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
								});

						turno = (usuarioid);
						imserver=true;
						return clientConector;

					}
				};

				mSocketServer.start();
			}
		});
		t.start();
	}

	private void initClient() {
		try {
			Thread.sleep(800);
		} catch (final Throwable t) {
			Debug.e(t);
		}
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					mServerConnector = new SocketConnectionServerConnector(
							new SocketConnection(new Socket(mServerIP,
									SERVER_PORT)),
							new ExampleServerConnectorListener());

					mServerConnector.registerServerMessage(
							FLAG_MESSAGE_SERVER_CONNECTION_CLOSE,
							ConnectionCloseServerMessage.class,
							new IServerMessageHandler<SocketConnection>() {
								@Override
								public void onHandleMessage(
										final ServerConnector<SocketConnection> pServerConnector,
										final IServerMessage pServerMessage)
										throws IOException {
									Batalla.this.finish();
								}
							});

					mServerConnector.registerServerMessage(
							FLAG_MESSAGE_SERVER_ADD_FACE,
							AddFaceServerMessage.class,
							new IServerMessageHandler<SocketConnection>() {
								@Override
								public void onHandleMessage(
										final ServerConnector<SocketConnection> pServerConnector,
										final IServerMessage pServerMessage)
										throws IOException {
									try {
										final AddFaceServerMessage addFaceServerMessage = (AddFaceServerMessage) pServerMessage;
										if (addFaceServerMessage.messageID
												/ countMessage > lastMessage) {
											lastMessage = addFaceServerMessage.messageID
													/ countMessage;
										turno = addFaceServerMessage.turn;
										if (turno == usuarioid) {
											final Sprite card = spritemanager.mFaces
													.get(addFaceServerMessage.mID);
											int pos = ((CardUserData) card
													.getUserData()).cardID;
											//toastl(pos+"");
											usuario.cartas.get(pos).vida = addFaceServerMessage.live;
											((CardUserData) card.getUserData()).cadena.Otexto[0]
													.setText("V:"
															+ addFaceServerMessage.live);
											spritemanager.cadenatiempop(
													"Tu turno!", 100, 100);
										} else {

											final Sprite card = spritemanager.mFaces
													.get(cartaEnemigo);
											((CardUserData) card.getUserData()).cadena.Otexto[0]
													.setText("V:"
															+ addFaceServerMessage.live);
											spritemanager.cadenatiempop(
													"Turno del oponente!", 100, 100);

										}

                                          //toast("cliente recibio ataque");
										Thread.sleep(1000);
										
										}
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									finally{
										semaforomessage = true;
									}
									

								}
							});

					mServerConnector.registerServerMessage(
							FLAG_MESSAGE_SERVER_MOVE_CARD,
							MoveCardServerMessage.class,
							new IServerMessageHandler<SocketConnection>() {
								@Override
								public void onHandleMessage(
										final ServerConnector<SocketConnection> pServerConnector,
										final IServerMessage pServerMessage)
										throws IOException {

									final MoveCardServerMessage moveCardServerMessage = (MoveCardServerMessage) pServerMessage;
									if (moveCardServerMessage.messageID
											/ countMessage > lastMessage) {
										try {
											lastMessage = moveCardServerMessage.messageID
													/ countMessage;

										
											int cid = moveCardServerMessage.mID;
											boolean user = (moveCardServerMessage.userID == usuarioid);
											if (!user) {
												cid = spritemanager.cardsID;
												CartaClass carta = new CartaClass();
												carta.ataque = moveCardServerMessage.attack;
												carta.vida = moveCardServerMessage.live;
												carta.defenza = moveCardServerMessage.defend;

												Batalla.this.spritemanager
														.addCard(
																Batalla.this.mEngine
																		.getScene(),
																spritemanager.cardsID,
																0,
																0,
																moveCardServerMessage.userID,
																moveCardServerMessage.cardID,
																moveCardServerMessage.spritecard,
																moveCardServerMessage.active,
																carta);
												cartaEnemigo = cid;
											} else {
												//toast("cliente acarta activa");
												usuario.hasCardActive = true;

											}
											Batalla.this.spritemanager
													.moveCard(cid, 0, 0, user);
                                           //  toast("cliente movio carta");
											Thread.sleep(1500);
										} catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										finally{
										semaforomessage = true;
										}
									}
								}
							});

					mServerConnector.registerServerMessage(
							FLAG_MESSAGE_SERVER_ID_USER,
							UserServerMessage.class,
							new IServerMessageHandler<SocketConnection>() {
								@Override
								public void onHandleMessage(
										final ServerConnector<SocketConnection> pServerConnector,
										final IServerMessage pServerMessage)
										throws IOException {
									try {
										final UserServerMessage userServerMessage = (UserServerMessage) pServerMessage;
										if (userServerMessage.messageID
												/ countMessage > lastMessage) {
											lastMessage = userServerMessage.messageID
													/ countMessage;
										int user = (usuarioid);
										int enemy = Integer
												.parseInt(usuario.enemy);

										if (((enemy == userServerMessage.enemy && user == userServerMessage.mID) || (enemy == userServerMessage.mID && user == userServerMessage.enemy))) {

											if (nplayers == 2) {
												
										
											}

										} else {
                                                    
											mServerConnector.getConnection()
													.terminate();
										}
										}
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							});

					mServerConnector.registerServerMessage(
							FLAG_MESSAGE_SERVER_LISTO,
							ListoServerMessage.class,
							new IServerMessageHandler<SocketConnection>() {
								@Override
								public void onHandleMessage(
										final ServerConnector<SocketConnection> pServerConnector,
										final IServerMessage pServerMessage)
										throws IOException {
									try {
										final ListoServerMessage userServerMessage = (ListoServerMessage) pServerMessage;
										
											

											// Batalla.this.moveFace(MoveCardServerMessage.mID,
											// MoveCardServerMessage.mX,
											// MoveCardServerMessage.mY);
										//toast("cliente recibio listo");
											listo = userServerMessage.listo;
											
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							});

					mServerConnector.registerServerMessage(
							FLAG_MESSAGE_SERVER_MDESTROY,
							MDestroyServerMessage.class,
							new IServerMessageHandler<SocketConnection>() {
								@Override
								public void onHandleMessage(
										final ServerConnector<SocketConnection> pServerConnector,
										final IServerMessage pServerMessage)
										throws IOException {
									final EngineLock engineLock = Batalla.this
											.getEngine().getEngineLock();
									
								
										final MDestroyServerMessage addFaceServerMessage = (MDestroyServerMessage) pServerMessage;
										if (addFaceServerMessage.messageID
												/ countMessage > lastMessage) {
											lastMessage = addFaceServerMessage.messageID
													/ countMessage;
										turno = addFaceServerMessage.turn;
										
										toastl("Eliminando, por favor espere...");
										if (turno == usuarioid) {
											
											vida = vida - 1;
											usuario.hasCardActive = false;
											

											//engineLock.lock();
											/*
											 * Now it is save to remove the
											 * entity!
											 */
											
											Batalla.this.getEngine().runOnUpdateThread(new Runnable() {
								                @Override
								                public void run() {
								                	Sprite card=null;
											try{
												
												card = spritemanager.mFaces
														.get(addFaceServerMessage.mID);
												int pos = ((CardUserData) card
														.getUserData()).cardID;
											//	toastl(pos+"mi carta");
												usuario.cartas.get(pos).vida = addFaceServerMessage.live;
											
											Cadena cad=((CardUserData) card.getUserData()).cadena;
													cad.eliminar();
											Batalla.this.getEngine().getScene()
													.unregisterTouchArea(card);
											Batalla.this.getEngine().getScene()
													.detachChild(card);
											card.dispose();
											card = null;
											}catch(Exception e){
												try{
												Batalla.this.getEngine().getScene()
												.detachChild(card);
												card.dispose();
												e.printStackTrace();
												}catch(Exception d){
												    d.printStackTrace();
												}
												
											}
											}});
											
											
											//engineLock.unlock();

											spritemanager.cadenatiempop(
													"Tu turno!", 100, 100);
										} else {
											
											spritemanager.cadenatiempop(
													"Turno del Oponente!", 100, 100);
											
											Batalla.this.getEngine().runOnUpdateThread(new Runnable() {
								                @Override
								                public void run() {
								                	Sprite card =null;
											try{
											
										 card = spritemanager.mFaces
													.get(cartaEnemigo);
											int pos = ((CardUserData) card
													.getUserData()).cardID;
											//toastl(pos+"oponente");
											usuario.cartas.get(pos).vida = addFaceServerMessage.live;
											

										
											((CardUserData) card.getUserData()).cadena
											.eliminar();
											Batalla.this.getEngine().getScene()
													.unregisterTouchArea(card);
											Batalla.this.getEngine().getScene()
													.detachChild(card);
											card.dispose();
											}catch(Exception e){
												try{
													Batalla.this.getEngine().getScene()
													.detachChild(card);
													card.dispose();
													e.printStackTrace();
													}catch(Exception d){
													    d.printStackTrace();
													}
											}
								                }});
											vidaEnemigo = vidaEnemigo - 1;
										//	engineLock.unlock();
											
										Sprite	card2 = spritemanager.mFaces
													.get(addFaceServerMessage.DID);
											int pos2 = ((CardUserData) card2
													.getUserData()).cardID;
											//toastl(pos2+"subir experiencia");
											usuario.cartas.get(pos2).experiencia = usuario.cartas
													.get(pos2).experiencia + 100;
											//toast("cambio la pos"+pos2);
											try{
											if (vidaEnemigo == 0) {
												GFinishClientMessage gFinishClientMessage = (GFinishClientMessage) Batalla.this.mMessagePool
														.obtainMessage(FLAG_MESSAGE_CLIENT_GFINISH);
												for(int i=0;i<countMessage;i++){ 
	                                            	clientmessageID++;
													gFinishClientMessage.set(
															usuarioid,
															Integer.parseInt(usuario.enemy),clientmessageID);
													Batalla.this.mServerConnector
															.sendClientMessage(gFinishClientMessage);
												}
												Batalla.this.mMessagePool
														.recycleMessage(gFinishClientMessage);
											}}catch(Exception e){
												e.printStackTrace();
											}

										}
										//toast("cliente destruyo");
										spritemanager.cadenavida(vida,
												vidaEnemigo);
										}
								
									}
									
								
							});

					mServerConnector.registerServerMessage(
							FLAG_MESSAGE_SERVER_GFINISH,
							GFinishServerMessage.class,
							new IServerMessageHandler<SocketConnection>() {
								@Override
								public void onHandleMessage(
										final ServerConnector<SocketConnection> pServerConnector,
										final IServerMessage pServerMessage)
										throws IOException {
									try {
										final GFinishServerMessage gFinishServerMessage = (GFinishServerMessage) pServerMessage;
										if (gFinishServerMessage.messageID
												/ countMessage > lastMessage) {
											lastMessage = gFinishServerMessage.messageID
													/ countMessage;
										if (usuarioid == gFinishServerMessage.mID) {
											spritemanager.cadenafijaop(
													"GANASTE",
													CAMERA_WIDTH / 2 - 100,
													CAMERA_HEIGHT / 2);
										
												String s = h.InsertarBatalla(
														usuario.torneo + "",
														 usuario.id, usuario.enemy );
												
												Thread.sleep(800);
												if(s.indexOf("aun")>=0){
													 s = h.InsertarBatalla(
															usuario.torneo + "",
															 usuario.id, usuario.enemy );
													
													Thread.sleep(800);
													
												}
																							
												s=s.substring(s.indexOf(";")+1,s.length());
												s=s.substring(0,s.indexOf(";"));
												System.out.println("Batalla convertida es "+s);
												batallaid = Integer.valueOf(s);
											
													
												
											h.ExperienciaJugador(
													usuarioid + "", (Integer.parseInt(usuario.experiencia)+100) + "");
											
											for (int i = 0; i < 5; i++) {
												//toast(""+usuario.cartas.get(i).id+" "+usuario.cartas.get(i).experiencia+" "+i);
												h.ExperienciaCarta(
														usuario.cartas.get(i).id
																+ "",
														usuario.cartas.get(i).experiencia
																+ "");

											}
											h.InsertarResultado(batallaid + "",
													100 + "", usuarioid + "",
													1 + "");
											h.InsertarResultado(batallaid + "",
													0 + "", usuario.enemy + "",
													0 + "");
											
                                                Thread.sleep(9000);
                                                Batalla.this.finish();
										} else {
											spritemanager.cadenafijaop(
													"PERDISTE",
													CAMERA_WIDTH / 2 - 100,
													CAMERA_HEIGHT / 2 );
											
											
											for (int i = 0; i < 5; i++) {
												h.ExperienciaCarta(
														usuario.cartas.get(i).id
																+ "",
														usuario.cartas.get(i).experiencia
																+ "");

											}
										}
										}
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							});
					mServerConnector.getConnection().start();
					empezado = true;

					final UserClientMessage userClientMessage = (UserClientMessage) Batalla.this.mMessagePool
							.obtainMessage(FLAG_MESSAGE_CLIENT_ID_USER);
					for(int i=0;i<countMessage;i++){ 
                    	clientmessageID++;
						userClientMessage.set(usuarioid,clientmessageID);

						// Batalla.this.mSocketServer.sendBroadcastServerMessage(MoveCardServerMessage);
						Batalla.this.mServerConnector
								.sendClientMessage(userClientMessage);
					}
					Batalla.this.mMessagePool.recycleMessage(userClientMessage);
				} catch (final Throwable t) {
					Debug.e(t);
				}
			}
		});
		t.start();
	}

	private void log(final String pMessage) {
		Debug.d(pMessage);
	}

	private void toast(final String pMessage) {
		this.log(pMessage);
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(Batalla.this, pMessage, Toast.LENGTH_SHORT)
						.show();
			}
		});
	}

		private void toastl(final String pMessage) {
		this.log(pMessage);
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(Batalla.this, pMessage, Toast.LENGTH_LONG)
						.show();
			}
		});
	}

	private class ExampleServerConnectorListener implements
			ISocketConnectionServerConnectorListener {
		@Override
		public void onStarted(final ServerConnector<SocketConnection> pConnector) {
			Batalla.this.toast("CLIENT: Connected to server.");
		}

		@Override
		public void onTerminated(
				final ServerConnector<SocketConnection> pConnector) {
			Batalla.this.toast("CLIENT: Disconnected from Server...");
			Batalla.this.finish();
			pConnector.terminate();
		}
	}

	private class ExampleServerStateListener implements
			ISocketServerListener<SocketConnectionClientConnector> {
		@Override
		public void onStarted(
				final SocketServer<SocketConnectionClientConnector> pSocketServer) {
			Batalla.this.toast("SERVER: Started.");
			toast("Esperando a cliente");
		}

		@Override
		public void onTerminated(
				final SocketServer<SocketConnectionClientConnector> pSocketServer) {
			Batalla.this.toast("SERVER: Terminated.");
		}

		@Override
		public void onException(
				final SocketServer<SocketConnectionClientConnector> pSocketServer,
				final Throwable pThrowable) {
			Debug.e(pThrowable);
			// Batalla.this.toast("SERVER: Exception: " + pThrowable);
		}
	}

	private class ExampleClientConnectorListener implements
			ISocketConnectionClientConnectorListener {
		@Override
		public void onStarted(final ClientConnector<SocketConnection> pConnector) {

			Batalla.this.nplayers = Batalla.this.nplayers + 1;
			Batalla.this.toast("SERVER: Client connected: "
					+ pConnector.getConnection().getSocket().getInetAddress()
							.getHostAddress());
            if(Batalla.this.nplayers==2){
            	Batalla.this.toast("Verificando usuarios, por favor espere");
            }
		
		}

		@Override
		public void onTerminated(
				final ClientConnector<SocketConnection> pConnector) {
			Batalla.this.nplayers = Batalla.this.nplayers - 1;
			Batalla.this.toast("SERVER: Client disconnected: "
					+ pConnector.getConnection().getSocket().getInetAddress()
							.getHostAddress());
			Batalla.this.finish();
		}
	}
}
