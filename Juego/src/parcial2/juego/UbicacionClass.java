package parcial2.juego;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class UbicacionClass {

	
	
	    private static UbicacionClass mInstance= null;

	    public LocationManager lm;
	    public Location oldLocation;
	    public Location locationA;
	    private LocationListener locationListener;
	    
	    protected UbicacionClass(Context c){
	    	
	        //Obtenemos una referencia al LocationManager
		    lm = (LocationManager)c.getSystemService(Context.LOCATION_SERVICE);
		 
		    
	        
		 
		    //Nos registramos para recibir actualizaciones de la posici�n
		    locationListener = new LocationListener() {
				
				@Override
				public void onStatusChanged(String provider, int status, Bundle extras) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onProviderEnabled(String provider) {
					if(provider.equals(LocationManager.GPS_PROVIDER)){
						
						lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
					}
				     System.out.println("encendio");	
				}
				
				@Override
				public void onProviderDisabled(String provider) {
					lm.removeUpdates(this);
					if(provider.equals(LocationManager.GPS_PROVIDER)){
					
					lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
					}
					else{
						lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
				  	
					}
					    
					
				}
				
				@Override
				public void onLocationChanged(Location location) {
		     		locationA=location;
		        if(!isBetterLocation(location, oldLocation)){
		            locationA=location;
		           oldLocation=location;
		           System.out.println("cambio loction");
		        }
		        else
		        {
		        	//locationA=oldLocation;
		        	
		        }
					
				}
			};
			
		    if(lm.isProviderEnabled(LocationManager.GPS_PROVIDER))
		    {
		    	lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
		    	locationA=lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		        
		    }
		    if(lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
		    {
		    	lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
		       	locationA=lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);   
		    }
	    	if(locationA!=null)
		    System.out.println("location es"+locationA.getProvider());
	    	else
	    	System.out.println("locaiotn es nll");
	    }
	    
	    
	    
	    
	    
////////////////////////////////7
		
private static final int TWO_MINUTES = 1000 * 60 * 2;

/** Determines whether one Location reading is better than the current Location fix
* @param location  The new Location that you want to evaluate
* @param currentBestLocation  The current Location fix, to which you want to compare the new one
*/
protected boolean isBetterLocation(Location location, Location currentBestLocation) {
if (currentBestLocation == null) {
// A new location is always better than no location
return true;
}

// Check whether the new location fix is newer or older
long timeDelta = location.getTime() - currentBestLocation.getTime();
boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
boolean isNewer = timeDelta > 0;

// If it's been more than two minutes since the current location, use the new location
// because the user has likely moved
if (isSignificantlyNewer) {
return true;
// If the new location is more than two minutes older, it must be worse
} else if (isSignificantlyOlder) {
return false;
}

// Check whether the new location fix is more or less accurate
int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
boolean isLessAccurate = accuracyDelta > 0;
boolean isMoreAccurate = accuracyDelta < 0;
boolean isSignificantlyLessAccurate = accuracyDelta > 200;

// Check if the old and new location are from the same provider
boolean isFromSameProvider = isSameProvider(location.getProvider(),
currentBestLocation.getProvider());

// Determine location quality using a combination of timeliness and accuracy
if (isMoreAccurate) {
return true;
} else if (isNewer && !isLessAccurate) {
return true;
} else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
return true;
}
return false;
}

/** Checks whether two providers are the same */
private boolean isSameProvider(String provider1, String provider2) {
if (provider1 == null) {
return provider2 == null;
}
if (provider2 == null) {
return provider1 == null;
}
return provider1.equals(provider2);
}

	    
	    
	    
	    
	    
	    
	    
	    
	    
	    

	    public static synchronized UbicacionClass getInstance(Context c){
	    	if(null == mInstance){
	    		mInstance = new UbicacionClass(c);
	    	}
	    	return mInstance;
	    }
	    
	    
	    
	    
	
}
