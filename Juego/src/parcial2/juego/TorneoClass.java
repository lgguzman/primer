package parcial2.juego;

import java.util.ArrayList;
import java.util.Scanner;

public class TorneoClass {
	String id;
	String nombre;
	String nivel;

	char cargado;
	
	public TorneoClass (String dato){
		if(dato.indexOf("�error")<0 && dato.indexOf("vacio")<0){
		try{
			
		String [] datos=split(dato,";");
		id=(datos[1]);
		nombre=(datos[2]);
		nivel=(datos[3]);
		
		if(datos[1]==null || datos[2]==null)
		cargado='n';
		else
		cargado='y';
		}catch(Exception e){
			
			cargado='n';
		}	}
		else{
			
			cargado='e';
		}
	}
	
	public static TorneoClass[] variosTorneos(String dato){
		TorneoClass []u=new TorneoClass[1];
		System.out.println(dato);
		Scanner s= new Scanner(dato);
		ArrayList<TorneoClass> array=new ArrayList<TorneoClass>();
		String linea;
		try{
		while(!(linea=s.nextLine()).equals("EOF") && linea!=null && linea.indexOf("�err")<0){
			TorneoClass torneo=new TorneoClass(linea);
			if(torneo!=null && torneo.cargado=='y')
			array.add(torneo);
			
		}
		u=new TorneoClass[array.size()];
		int c=0;
		
		for(TorneoClass user:array){

			u[c]=user;
					c++;
		}}catch(Exception e){
			System.out.println("error en varios torneo"+e.getMessage());
		}
		return u;
	}
   
	
	public String[] split(String s, String r){
		System.out.println("split :"+s);
		String copy=""+s;
		String array[]=new String[10];
		int i;
		int cont=0;
		while((i=copy.indexOf(r))!=-1){
			array[cont]=copy.substring(0,i);
			cont++;
			copy=copy.substring(i+1, copy.length());
		}
		array[cont]=copy;
		
		return array;
	}
	
	public String toString(){
		
		try{StringBuilder sb=new StringBuilder();
		sb.append(";");
		sb.append(this.id);
		sb.append(";");
		sb.append(this.nombre);
		sb.append(";");
		sb.append(this.nivel);
	
		return sb.toString();
		}catch(Exception e){
			return e.getMessage();
		}
	
		
		
		
	}

}
