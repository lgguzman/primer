package parcial2.juego;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.graphics.Picture;
import android.util.Log;



public class HttpHandler {
	String response;
	String url;
	ArrayList<String> d;
	
	
	private static HttpHandler mInstance= null;
	
    public static synchronized HttpHandler getInstance(){
    	if(null == mInstance){
    		mInstance = new HttpHandler();
    	}
    	return mInstance;
    }
	
	protected HttpHandler() {
		
	}
	public String Petici�n(String u, ArrayList<String> dat){
		response="aun no";
		url=u;
		d=dat;
		  Thread thr = new Thread(new Runnable() {
			  @Override
			  public void run() {
			  //Enviamos el texto escrito a la funcion
				 
			  response=EnviarDatos(url,d);
			  }
			  });
			 //Arrancamos el Hilo
			 thr.start();
			long limit=0;
			 long time=System.currentTimeMillis();
			    while(response.equals("aun no") && limit < 10000){
			    	limit=System.currentTimeMillis()-time;
			    	
			    }
			
			    return response;
	}
	
	public String EnviarDatos(String url, ArrayList<String> datos){
		  try {
			   
		         // Crear un cliente por defecto 
		         HttpClient mClient = new DefaultHttpClient();

		         // Indicar la url
		         StringBuilder sb=new StringBuilder(url);

		         // Establecer la conexi�n despu�s de indicar la url
		         HttpPost mpost = new HttpPost(sb.toString());

		         // NameValuePair : Es una clase simple que encapsula el nombre del atributo y el valor que contiene.
		         // Creamos una lista de 8 atributos
		         List nameValuepairs = new ArrayList(d.size()/2);

		         // A�adimos los elementos a la lista
		         for(int i=0;i<d.size();i=i+2){
		        	 System.out.println(d.get(i) +"=>"+d.get(i+1));
		         nameValuepairs.add(new BasicNameValuePair(d.get(i).toString(),d.get(i+1).toString()));
		         }
		        

		         // UrlEncodedFormEntity : Codificamos la lista para el envio por post
		         mpost.setEntity(new UrlEncodedFormEntity(nameValuepairs));
		         
		         // Ejecutamos la solicitud y obtenemos una respuesta
		         HttpResponse responce = mClient.execute(mpost);
		         
		         //  Obtenemos el contenido de la respuesta
		         HttpEntity entity = responce.getEntity();

		         // Convertimos el stream a un String 
		         BufferedReader buf = new BufferedReader(new InputStreamReader(entity.getContent()));
		         StringBuilder sb1 = new StringBuilder();
		         String line = null;

		         while ((line = buf.readLine()) != null) {
		        //	 System.out.print(line);
		             sb1.append(line+"\r\n");
		         }
		         
		         String salida=sb1.toString();
		      // System.out.println(salida);
		        if(salida.indexOf("<")!=-1)
		         return salida.substring(0,salida.indexOf("<"));
		        else
		        	return salida;
		        // return sb1.toString();

		     } catch (Exception e) {
		         //Si se produce un error, lo mostramos
		         Log.w(" error ", e.toString());
		       
		         return e.getMessage();
		     }
	}

	public UsuarioClass login(String user, String pass){
		 ArrayList<String> a=new ArrayList<String>();
		 a.add("Correo");
		 a.add(user);
		 a.add("Contrasena");
		 a.add(pass);
		 
	     return new UsuarioClass(this.Petici�n("http://www.juegowebmovil.hol.es/loginM.php", a));
		
	}
    
	public UsuarioClass[] conectados(String id){
		
		 ArrayList<String> a=new ArrayList<String>();
		 a.add("idUsuario");
		 a.add(id);
		 return UsuarioClass.variosUsuarios(this.Petici�n("http://www.juegowebmovil.hol.es/conectadosM.php", a));
	}
	
	public CartaClass[] buscar(String id){
		
		 ArrayList<String> a=new ArrayList<String>();
		 a.add("idCarta");
		 a.add(id);
		 return CartaClass.variasCartasV(this.Petici�n("http://www.juegowebmovil.hol.es/buscar.php", a));
	}
	
	public String ExperienciaCarta(String id, String experiencia){
		 ArrayList<String> a=new ArrayList<String>();
		 a.add("idUsuario");
		 a.add(id);
		 a.add("Experiencia");
		 a.add(experiencia);
		
		 return (this.Petici�n("http://www.juegowebmovil.hol.es/ExperienciaCarta.php", a));
	}
	public String ExperienciaJugador(String id, String experiencia){
		 ArrayList<String> a=new ArrayList<String>();
		 a.add("idUsuario");
		 a.add(id);
		 a.add("Experiencia");
		 a.add(experiencia);
		
		 return (this.Petici�n("http://www.juegowebmovil.hol.es/ExperienciaJugador.php", a));
	}
	
	public String InsertarResultado(String id, String puntaje, String user, String gano){
		 ArrayList<String> a=new ArrayList<String>();
		 a.add("idBatalla");
		 a.add(id);
		 a.add("puntaje");
		 a.add(puntaje);
		 a.add("idUser");
		 a.add(user);
		 a.add("gano");
		 a.add(gano);
		
		 return (this.Petici�n("http://www.juegowebmovil.hol.es/InsertarResultado.php", a));
	}
	public String InsertarBatalla(String id, String user, String user2){
		 ArrayList<String> a=new ArrayList<String>();
		 a.add("idTorneo");
		 a.add(id);
		 a.add("idUser1");
		 a.add(user);
		 a.add("idUser2");
		 a.add(user2);
		
		 return (this.Petici�n("http://www.juegowebmovil.hol.es/InsertarBatalla.php", a));
	}
	
	public UsuarioClass[] isUser(String iduser, String idtorn){
		
		 ArrayList<String> a=new ArrayList<String>();
		 a.add("idUsuario");
		 a.add(iduser);
		 a.add("idTorneo");
		 a.add(idtorn);
		 return UsuarioClass.variosUsuarios(this.Petici�n("http://www.juegowebmovil.hol.es/isUserTorneo.php", a));
	}
	
	public TorneoClass[] torneos(){
		ArrayList<String> a=new ArrayList<String>();
		
		 return TorneoClass.variosTorneos(this.Petici�n("http://www.juegowebmovil.hol.es/Torneos.php", a));
	}
	
	public UsuarioClass[] registradosTorneo(String idUsuario, String idTorneo){
		
		 ArrayList<String> a=new ArrayList<String>();
		 a.add("idUsuario");
		 a.add(idUsuario);
		 a.add("idTorneo");
		 a.add(idTorneo);
		 return UsuarioClass.variosUsuarios(this.Petici�n("http://www.juegowebmovil.hol.es/UserTorneo.php", a));
	}
	
	public CartaClass[] cartasV(String id){
		
		 ArrayList<String> a=new ArrayList<String>();
		 a.add("idUsuario");
		 a.add(id);
		 return CartaClass.variasCartasV(this.Petici�n("http://www.juegowebmovil.hol.es/CartasUser.php", a));
		 
	}
	
	public ArrayList<CartaClass> cartas(String id){
		
		 ArrayList<String> a=new ArrayList<String>();
		 a.add("idUsuario");
		 a.add(id);
		 System.out.println("Aqui entre");
		 return CartaClass.variasCartas(this.Petici�n("http://www.juegowebmovil.hol.es/CartasUser.php", a));
		 
	}

	public String logout(String id){
		 ArrayList<String> a=new ArrayList<String>();
		 a.add("idUsuario");
		 a.add(id);
		
		 return (this.Petici�n("http://www.juegowebmovil.hol.es/logoutM.php", a))+ (this.Petici�n("http://www.juegowebmovil.hol.es/ActivoOff.php", a));
	}
	
	public String updateIn(String id){
		 ArrayList<String> a=new ArrayList<String>();
		 a.add("idUsuario");
		 a.add(id);
		 return (this.Petici�n("http://www.juegowebmovil.hol.es/updateIn.php", a));
	}
	
	public String ActualizarU(String id, String Lat, String Lon){
		 ArrayList<String> a=new ArrayList<String>();
		 a.add("idUsuario");
		 a.add(id);
		 a.add("Lat");
		 a.add(Lat);
		 a.add("Lon");
		 a.add(Lon);
		 return (this.Petici�n("http://www.juegowebmovil.hol.es/ActualizarU.php", a));
	}
	
	public String ipServer(String IP, String enemy, String server){
		ArrayList<String> a=new ArrayList<String>();
		a.add("idServer");
		a.add(server);
		a.add("idCliente");
		a.add(enemy);
		a.add("ip");
		a.add(IP);
		return this.Petici�n("http://www.juegowebmovil.hol.es/ipServer.php", a);
	}
	


	public String activoOff(String id){
		 ArrayList<String> a=new ArrayList<String>();
		 a.add("idUsuario");
		 a.add(id);
		
		 return (this.Petici�n("http://www.juegowebmovil.hol.es/ActivoOff.php", a));
	}
}
