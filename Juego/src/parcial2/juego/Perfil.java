package parcial2.juego;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class Perfil extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_perfil);
		Bundle b =getIntent().getExtras();
		UsuarioClass usuario=new UsuarioClass(b.getString("user"));
		EditText nombre=(EditText)findViewById(R.id.nombre);
		EditText tarea=(EditText)findViewById(R.id.tarea);
		EditText experencia=(EditText)findViewById(R.id.experiencia);
		nombre.setText(usuario.nombre);
		experencia.setText(usuario.experiencia);
		tarea.setText("Latitud= "+usuario.lat+"\nLongitud= "+usuario.lon+"\nMis cartas");
		if(usuario.id.equals(UserSession.getInstance().usuario.id)){
			usuario=UserSession.getInstance().usuario;
		for(int i=0;i<usuario.cartas.size();i++){
			tarea.setText(tarea.getText()+usuario.cartas.get(i).nombre);
			
		}}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.perfil, menu);
		return true;
	}

}
