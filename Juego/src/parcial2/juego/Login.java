package parcial2.juego;




import android.location.Location;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends Activity {
	private EditText user,pass;
	private Button b;
	HttpHandler h;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login); 
		h=HttpHandler.getInstance();
		user=(EditText) findViewById(R.id.editText1);
		pass=(EditText) findViewById(R.id.editText2);
		b=(Button) findViewById(R.id.button1);
		UsuarioClass u=UserSession.getInstance().usuario;
		if( u!=null){
			h.logout(u.id);
			 UserSession.destroy();
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}
	
	 public void Enviar(View view){
     		UsuarioClass u= h.login(user.getText().toString(), pass.getText().toString());
		     if(u!=null && u.cargado=='y'){
		     u.cartas=h.cartas(u.id);
		     if(u.cartas!=null && !u.cartas.isEmpty()){
		    	 Location l =UbicacionClass.getInstance(this).locationA;	
		     Intent intento = new Intent(this, FreePlay.class);
		     UserSession.getInstance().usuario=u;
		    finish();
		     startActivity(intento);
		   }
		   else{
                
                Toast.makeText(
						Login.this, 
						"No se cargaron las cartas",
						Toast.LENGTH_LONG).show();
                h.logout(u.id);

		   }
		     }
		     else{
		    	 if(u!= null && u.cargado=='e'){
		    	 user.setText("");
		    	 pass.setText("");
		 		Toast.makeText(
						Login.this, 
						"Nombre de usuario o contrase�a incorrectos",
						Toast.LENGTH_LONG).show();
		     }else{
		    	 
		 		Toast.makeText(
						Login.this, 
						"Error de conexi�n",
						Toast.LENGTH_LONG).show();
		     }
		    	 }

			 
		 }

}
