package parcial2.juego;

import org.andengine.engine.Engine.EngineLock;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.text.AutoWrap;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.opengl.font.BitmapFont;
import org.andengine.util.HorizontalAlign;
import org.andengine.util.color.Color;

import android.app.Activity;

public class Cadena implements Constant {
	
	float x;
	float y;
	String texto;
	Batalla context;
    BitmapFont mBitmapFont;
    Text[] Otexto= new Text[3];
    String []Stexto={"","",""};
	
	public void tiempo(){
		
		
		
        (new Thread(new Runnable(){

			@Override
			public void run() {
				final EngineLock engineLock = context.getEngine().getEngineLock();
				
					
					 Text bitmapText = new Text(x, y, mBitmapFont, texto, new TextOptions(HorizontalAlign.CENTER), context.getVertexBufferObjectManager());
                   bitmapText.setColor(Color.RED);
					context.getEngine().getScene().attachChild(bitmapText);
					
					try {
					Thread.sleep(2000);
			    	
					engineLock.lock();
					/* Now it is save to remove the entity! */
					context.getEngine().getScene().detachChild(bitmapText);
					bitmapText.dispose();
					bitmapText = null;

					
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally{
					try{
					engineLock.unlock();}
					catch(Exception e){
						e.printStackTrace();
					}
				}
			
				
			}
        	
        	
        })).start();
		
	}
	
	
	public void Fija(){
		
		
		
     
				try {
					
					 Text bitmapText = new Text(x, y, mBitmapFont, texto, new TextOptions(HorizontalAlign.CENTER), context.getVertexBufferObjectManager());
                      bitmapText.setColor(Color.RED);
					context.getEngine().getScene().attachChild(bitmapText);
					
										
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			
		
	}
	
    public void carta(final Scene scene){

				try {

					 Otexto[0] = new Text(wcard+x, y, mBitmapFont, Stexto[0], new TextOptions(), context.getVertexBufferObjectManager());
					 Otexto[0].setColor(Color.RED);
					 Otexto[1] = new Text(wcard+x, y+32, mBitmapFont, Stexto[1], new TextOptions(), context.getVertexBufferObjectManager());
					 Otexto[2] = new Text(wcard+x, y+64, mBitmapFont, Stexto[2], new TextOptions(), context.getVertexBufferObjectManager());
					scene.attachChild(Otexto[0]);
					scene.attachChild(Otexto[1]);
					scene.attachChild(Otexto[2]);
					
										
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

		
	}
    
    
    public void eliminar(){
    	
    	for (int i = 0; i < 3; i++) {
			
		
    	try{
    
	//	engineLock.lock();
		/* Now it is save to remove the entity! */
    		
                	try{
                	if(Otexto!=null && Otexto[0]!=null && Otexto[1]!=null && Otexto[2]!=null){
            		context.getEngine().getScene().detachChild(Otexto[0]);
            		Otexto[0].dispose();
            		Otexto[0] = null;
            		context.getEngine().getScene().detachChild(Otexto[1]);
            		Otexto[1].dispose();
            		Otexto[1] = null;
            		context.getEngine().getScene().detachChild(Otexto[2]);
            		Otexto[2].dispose();
            		Otexto[2] = null;
                    Otexto=null;}
                	}catch(Exception e)
                	{
                		e.printStackTrace();
                	}
                

        Runtime.getRuntime().gc();
		//engineLock.unlock();
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	finally{
    		}
    	}
    }
   
}
