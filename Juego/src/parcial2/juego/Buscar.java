package parcial2.juego;

import java.util.Random;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

public class Buscar extends Activity {
	UsuarioClass usuario;

	HttpHandler h;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		h = HttpHandler.getInstance();
		usuario=UserSession.getInstance().usuario;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_buscar);
		Random r = new Random();
		if(r.nextBoolean()){
		r.nextInt(11);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.buscar, menu);
		return true;
	}
	public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        Intent intento;
        switch (itemId) {
                case R.id.btTorneos:
                	  intento = new Intent(this, Torneo.class);
                	  finish();
                	  startActivity(intento);
                        break;
                case R.id.btMiscartas:
                	 intento = new Intent(this, Carta.class);
                	 finish();
               	  startActivity(intento);
                        break;
                case R.id.btFreePlay:
               	 intento = new Intent(this, FreePlay.class);
               	 finish();
              	  startActivity(intento);
                       break;
                case R.id.btLogout:
                	UserSession.destroy();
            		h.logout(usuario.id);
            		intento = new Intent(this, Login.class);
            	     finish();
            	     startActivity(intento);
                	break;
                }
        return super.onOptionsItemSelected(item);
    }
	
	
}
