package parcial2.juego;

import java.util.ArrayList;
import java.util.Scanner;



public class UsuarioClass {
	
	String id;
	String nombre;
	String correo;
	String lat;
	String lon;
	String experiencia;
	String enemy;
	boolean hasCardActive;
	int cardActive;
	int torneo;
	ArrayList<CartaClass>cartas;
	char cargado;
	
	public UsuarioClass (String dato){
		if(dato.indexOf("�error")<0 && dato.indexOf("vacio")<0){
		try{
			
		String [] datos=split(dato,";");
		id=(datos[1]);
		nombre=(datos[2]);
		correo=(datos[3]);
		experiencia=(datos[4]);
		lat=(datos[5]);
		lon=(datos[6]);
		if(datos[1]==null || datos[2]==null)
		cargado='n';
		else
		cargado='y';
		}catch(Exception e){
			
			cargado='n';
		}	}
		else{
			
			cargado='e';
		}
	}
	
	public static UsuarioClass[] variosUsuarios(String dato){
		UsuarioClass []u=new UsuarioClass[1];
		Scanner s= new Scanner(dato);
		ArrayList<UsuarioClass> array=new ArrayList<UsuarioClass>();
		String linea;
		System.out.println(dato);
		try{
		while(!(linea=s.nextLine()).equals("EOF") && linea!=null && linea.indexOf("�err")<0){
			UsuarioClass usuario=new UsuarioClass(linea);
			if(usuario!=null && usuario.cargado=='y')
			array.add(usuario);
			
		}
		u=new UsuarioClass[array.size()];
		int c=0;
		
		for(UsuarioClass user:array){

			u[c]=user;
					c++;
		}}catch(Exception e){
			System.out.println("error en varios usuarios"+e.getMessage());
		}
		return u;
	}
   
	
	public String[] split(String s, String r){
		System.out.println("split :"+s);
		String copy=""+s;
		String array[]=new String[10];
		int i;
		int cont=0;
		while((i=copy.indexOf(r))!=-1){
			array[cont]=copy.substring(0,i);
			cont++;
			copy=copy.substring(i+1, copy.length());
		}
		array[cont]=copy;
		
		return array;
	}
	
	public String toString(){
		
		try{StringBuilder sb=new StringBuilder();
		sb.append(";");
		sb.append(this.id);
		sb.append(";");
		sb.append(this.nombre);
		sb.append(";");
		sb.append(this.correo);
		sb.append(";");
		sb.append(this.experiencia);
		sb.append(";");
		sb.append(this.lat);
		sb.append(";");
		sb.append(this.lon);
		return sb.toString();
		}catch(Exception e){
			return e.getMessage();
		}
	
		
		
		
	}
}
