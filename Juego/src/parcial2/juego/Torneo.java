package parcial2.juego;

import parcial2.juego.FreePlay.AdapterElements;
import android.location.Location;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class Torneo extends Activity {
	TextView bienv;
	UsuarioClass usuario;
	TorneoClass torneo;
	static TorneoClass[] lista;
	HttpHandler h;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_torneo);
		actualizar(null);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.torneo, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        Intent intento;
        switch (itemId) {
                case R.id.btFreePlay:
                	  intento = new Intent(this, FreePlay.class);
                	  finish();
                	  startActivity(intento);
                        break;
                case R.id.btMiscartas:
                	 intento = new Intent(this, Carta.class);
                	 finish();
               	  startActivity(intento);
                        break;

                }
        return super.onOptionsItemSelected(item);
    }
	
	public void actualizar(View view){
		bienv=(TextView) findViewById(R.id.bienvenido);
		h = HttpHandler.getInstance();
		usuario=UserSession.getInstance().usuario;
		lista=h.torneos();
		
		if(lista[0]!=null)
		{bienv.setText("Bienvenido: "+usuario.nombre);
		ListView list=(ListView) findViewById(R.id.lista);
		 AdapterElements adapter = new AdapterElements(this);
		list.setAdapter(adapter);}
		else
			bienv.setText("Bienvenido: "+usuario.nombre+"\nNo hay torneos disponibles");
		
			}
	
	class AdapterElements extends ArrayAdapter<Object> {
		Activity c;
		
		public AdapterElements(Activity context) {
    		super(context, R.layout.item2, lista);
    		this.c = context;
		}
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			
			LayoutInflater inflater = c.getLayoutInflater();
			View view = inflater.inflate(R.layout.item2, null);
			
			TextView tTitle =(TextView) view.findViewById(R.id.tittle);
			tTitle.setText(lista[position].nombre);
			
			TextView tdes =(TextView) view.findViewById(R.id.description);
			tdes.setText("Nivel: "+lista[position].nivel);
			Button bat=(Button) view.findViewById(R.id.batallar);
			
			Button mas =(Button) view.findViewById(R.id.mas);
			mas.setId(position);
			mas.setOnClickListener(new OnClickListener() {
			      @Override
			   public void onClick(View view) {
			    // TODO Auto-generated method stub
			    		 Intent intento = new Intent(view.getContext(), Registrados.class);
			    	     intento.putExtra("torneo", lista[view.getId()].toString());
			    	     
			    	     view.getContext().startActivity(intento);		    	  
			       }
			  });
			
			
			
			
			return view;
		}
		}
}
