package parcial2.juego;

public class GameLogic {
	
	GraphicCard user1;
	GraphicCard user2;
	
	
	
	public GameLogic(){		
	user1=new GraphicCard();
	user2=new GraphicCard();
	user1.mID=-1;
	user1.mID=-1;
		
	}
	
	public boolean attack(int userID, boolean power){		
		GraphicCard temp;
		if(userID!=user1.userID)
		{temp=user1;
		user1=user2;
		user2=temp;}
		if (!power)
		user2.live=Math.max(user2.live-Math.max(user1.attack-user2.defend,0),0);
		else
		user2.live=Math.max(user2.live-Math.max(user1.power-user2.defend,0),0);
		return (user2.live==0)?true:false;	 
		
		
	}
	
 	public boolean updateGraphicCard(final int pID, final float pX, final float pY, final int userID,final int live,final int power,final int defend, final int attack, int cardID, boolean active){
 		GraphicCard temp=user2;
		if(user1.mID==-1)
		{temp=user1;}
		
		temp.mID = pID;
		temp.mX = pX;
		temp.mY = pY;
		temp.userID= userID;
		temp.live=live;
		temp.power=power;
		temp.defend=defend;
		temp.attack=attack;
		temp.cardID=cardID;
		temp.active=active;
		
		return (user1.mID!=-1 && user2.mID!=-1);
 	}
	
public class GraphicCard{
	
	public int mID;
	public float mX;
	public float mY;
	public int userID;
	public int live;
	public int power;
	public int defend;
	public int attack;
    public boolean active;
    public int cardID;
	
}
	
}
