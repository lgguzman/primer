package parcial2.juego;

import java.io.IOException;
import java.util.ArrayList;

import org.andengine.audio.music.Music;
import org.andengine.audio.music.MusicFactory;
import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.engine.Engine.EngineLock;
import org.andengine.entity.Entity;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.AutoWrap;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.opengl.font.BitmapFont;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.util.HorizontalAlign;
import org.andengine.util.debug.Debug;


import android.util.SparseArray;

public class SpriteManager implements Constant {
	
	
	

	private BitmapTextureAtlas mBackgroundTexture;
	private ITextureRegion mBackgroundTextureRegion;
    private ArrayList<BitmapTextureAtlas> mCardsTextureAtlas;
    private BitmapFont mBitmapFont;
    private BitmapFont mBitmapFontG;
    private ArrayList<ITextureRegion> cards; 
    public Batalla context;
    public Text vida;
    public Text vidaEnemigo;
	public int cardsID=0;
	Music mMusic;
	//============================================================

	
	public final SparseArray<Sprite> mFaces = new SparseArray<Sprite>();
	
	public void loadCards(){
		cards= new ArrayList<ITextureRegion>();
		mCardsTextureAtlas= new ArrayList<BitmapTextureAtlas>();
		
		for (int i = 0; i < 10; i++) {
			
			    mCardsTextureAtlas.add(new BitmapTextureAtlas(context.getTextureManager(), wcard,hcard, TextureOptions.BILINEAR_PREMULTIPLYALPHA));
			    String h=((i+1)<10)?"0":"";
				cards.add(BitmapTextureAtlasTextureRegionFactory.createFromAsset(mCardsTextureAtlas.get(i), context, h+(i+1)+".jpg",0,0));
				mCardsTextureAtlas.get(i).load();
			
		}
	
		
	}

	 public void loadBackground(Scene scene,int card[], int userID, ArrayList<CartaClass> cartas){
		
		//scene.setBackground(new Background(0.09804f, 0.6274f, 0.8784f));
			scene.attachChild(new Entity());
			/* We allow only the server to actively send around messages. */
			scene.setBackgroundEnabled(false);
			scene.getChildByIndex(LAYER_BACKGROUND).attachChild(new Sprite(0, 0, this.mBackgroundTextureRegion, context.getVertexBufferObjectManager()));
            chargeCard(scene,card,userID,cartas);
            scene.attachChild(vida);
            scene.attachChild(vidaEnemigo);
		 
	 } 
	 
	 
	 public void chargeCard(Scene scene,int card[], int userID, ArrayList<CartaClass> cartas){
		 for(int i=0;i<card.length;i++){
			addCard(scene,cardsID,(context.CAMERA_WIDTH-540)*0.5f+i*90+i*30,context.CAMERA_HEIGHT-120*0.5f,userID,i,card[i]-1,false,(cartas==null)?null:cartas.get(i));
			
		 }
		 
	 }
	 
	 public void sound(){
		 MusicFactory.setAssetBasePath("img/");
			try {
				this.mMusic = MusicFactory.createMusicFromAsset(context.getMusicManager(), context, "BATALLA.ogg");
				this.mMusic.setLooping(true);
			} catch (final IOException e) {
				Debug.e(e);
			}
		//	mExplosionSound.setLooping(true);
		//	mExplosionSound.setLoopCount(5);
			mMusic.setLooping(true);
			mMusic.play();
			
		 
	 }
	 
		public void addCard(Scene scene,final int pID, final float pX, final float pY, int userID,int ncard, int spritecard, boolean active,CartaClass cartaclase) {
             try{
			final Sprite card = new Sprite(0, 0, this.cards.get(spritecard), context.getVertexBufferObjectManager());
			card.setPosition(pX - card.getWidth() * 0.5f, pY - card.getHeight() * 0.5f);
			
			this.mFaces.put(pID, card);
			scene.registerTouchArea(card);
			scene.attachChild(card);
			Cadena cadena= new Cadena();
			if(cartaclase!=null){
				cadena =new Cadena();
				cadena.mBitmapFont=mBitmapFontG;
				cadena.x=card.getX();
				cadena.y=card.getY();
				cadena.context=this.context;
				cadena.Stexto[0]="V:"+cartaclase.vida;
				cadena.Stexto[1]="A:"+cartaclase.ataque;
				cadena.Stexto[2]="D:"+cartaclase.defenza;
				cadena.carta(scene);
				
			}
			CardUserData cud=new CardUserData(userID,pID,ncard,spritecard,active,cadena);
		
			card.setUserData(cud);
			
			cardsID++;}
             catch(Exception e){
            	 e.printStackTrace();
             }
		}
	public SpriteManager(Batalla context){
		this.context=context;
		
		
	}
	
	public void chargeTexture(){
		
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("img/");
	
		loadCards();
		this.mBackgroundTexture = new BitmapTextureAtlas(context.getTextureManager(), 1024,512);
	
		this.mBackgroundTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBackgroundTexture, context, "HC_Campo.jpg", 0, 0);
		this.mBackgroundTexture.load();
		this.mBitmapFontG = new BitmapFont(context.getTextureManager(), context.getAssets(), "font/BitmapFontWithKerning.fnt");
		this.mBitmapFontG.load();
		this.mBitmapFont = new BitmapFont(context.getTextureManager(), context.getAssets(), "font/BitmapFont.fnt");
		this.mBitmapFont.load();
	    vida = new Text(0, 0, mBitmapFont, "Vida :5", new TextOptions(HorizontalAlign.CENTER), context.getVertexBufferObjectManager());
	    vidaEnemigo = new Text(0, 42, mBitmapFont, "Vida Oponente :5", new TextOptions(HorizontalAlign.CENTER), context.getVertexBufferObjectManager());

	   
		
		
	}
	
	public void cadenatiempop(String s, float x, float y ){
    try{
	Cadena cadena= new Cadena();
	cadena.x=x;
	cadena.y=y;
	cadena.texto=s;
	cadena.mBitmapFont=mBitmapFont;
	cadena.context=context;
	cadena.tiempo();
	cadena=null;
    }
    catch(Exception e){
    	e.printStackTrace();
    }
		
	}
	
	public void cadenatiempog(String s, float x, float y ){
  try{
	Cadena cadena= new Cadena();
	cadena.x=x;
	cadena.y=y;
	cadena.texto=s;
	cadena.mBitmapFont=mBitmapFontG;
	cadena.context=context;
	cadena.tiempo();
	cadena=null;
  }
  catch(Exception e){
  	e.printStackTrace();
  }
		
	}
	
	
	public void cadenafijaop(String s, float x, float y ){
try{
	Cadena cadena= new Cadena();
	cadena.x=x;
	cadena.y=y;
	cadena.texto=s;
	cadena.mBitmapFont=mBitmapFont;
	cadena.context=context;
	cadena.Fija();
    cadena=null;
}
catch(Exception e){
	e.printStackTrace();
}	
	}
	
	public void cadenafijag(String s, float x, float y ){
  try{
	Cadena cadena= new Cadena();
	cadena.x=x;
	cadena.y=y;
	cadena.texto=s;
	cadena.mBitmapFont=mBitmapFontG;
	cadena.context=context;
	cadena.Fija();
	cadena=null;
  }
  catch(Exception e){
  	e.printStackTrace();
  }
		
	}
	
	public void cadenavida(int vida, int enemigo){
  try{
		this.vida.setText("Vida: "+vida);
  this.vidaEnemigo.setText("Vida Oponente: "+enemigo);
  }
  catch(Exception e){
  	e.printStackTrace();
  }
		
	}
	


	public void moveCard(final int pID, final float pX, final float pY, boolean user) {
	try{
		/* Find and move the face. */
		final Sprite card = this.mFaces.get(pID);
		CardUserData c= (CardUserData)card.getUserData();
		//Cadena cadena =new Cadena();
		//cadena.mBitmapFont=mBitmapFontG;
		//cadena.context=this.context;
		//cadena.Stexto[0]=c.cadena.Stexto[0];
		//cadena.Stexto[1]=c.cadena.Stexto[1];
		//cadena.Stexto[2]=c.cadena.Stexto[2];
		
		
		// c.cadena.eliminar();
		 try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(user)
			{card.setPosition(pX+context.CAMERA_WIDTH*0.5f - card.getWidth() * 0.5f, pY+context.CAMERA_HEIGHT*0.5f);

			//cadena.x=pX+context.CAMERA_WIDTH*0.5f - card.getWidth() * 0.5f;
			//cadena.y=pY+context.CAMERA_HEIGHT*0.5f;
             c.cadena.Otexto[0].setPosition(wcard+pX+context.CAMERA_WIDTH*0.5f - card.getWidth() * 0.5f, pY+context.CAMERA_HEIGHT*0.5f);
             c.cadena.Otexto[1].setPosition( c.cadena.Otexto[0].getX(), c.cadena.Otexto[0].getY()+32);
             c.cadena.Otexto[2].setPosition( c.cadena.Otexto[0].getX(), c.cadena.Otexto[0].getY()+64);
			//cadena.carta(context.getEngine().getScene());

			}
		else
			{
			
			card.setPosition(pX+context.CAMERA_WIDTH*0.5f - card.getWidth() * 0.5f, pY+context.CAMERA_HEIGHT*0.5f-card.getHeight());
			//cadena.x=pX+context.CAMERA_WIDTH*0.5f - card.getWidth() * 0.5f;
			//cadena.y= pY+context.CAMERA_HEIGHT*0.5f-card.getHeight();
			  c.cadena.Otexto[0].setPosition(wcard+pX+context.CAMERA_WIDTH*0.5f - card.getWidth() * 0.5f, pY+context.CAMERA_HEIGHT*0.5f-card.getHeight());
	             c.cadena.Otexto[1].setPosition( c.cadena.Otexto[0].getX(), c.cadena.Otexto[0].getY()+32);
	             c.cadena.Otexto[2].setPosition( c.cadena.Otexto[0].getX(), c.cadena.Otexto[0].getY()+64);
		//	cadena.carta(context.getEngine().getScene());
			}
	//	c.cadena=cadena;
	  }
    catch(Exception e){
    	e.printStackTrace();
    }
	}
	
	
	// ===================================
	//  user dATA
	// =====================================

	public class CardUserData{
		int ownerID;
		int cardID;
		int GraphicID;
		int spritecard;
		boolean active;
		Cadena cadena;
		
		public CardUserData(int ownerID, int GraphicID,int cardID,int spritecard, boolean active, Cadena cadena){
			this.ownerID=ownerID;
			this.cardID=cardID;
			this.active=active;
			this.spritecard=spritecard;
			this.GraphicID=GraphicID;
			this.cadena= cadena;
		}
		
	}
	
}


