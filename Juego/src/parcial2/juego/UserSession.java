package parcial2.juego;

public class UserSession {
	
	private static UserSession mInstance= null;

	public static UsuarioClass usuario;
	protected UserSession(){

		
	}
	
	
    public static synchronized UserSession getInstance(){
    	if(null == mInstance){
    		mInstance = new UserSession();
    	}if(usuario!=null)
         HttpHandler.getInstance().updateIn(usuario.id);
    	return mInstance;
    }
    
    public static synchronized void destroy(){
    	usuario=null;
    	
    }

}
