package parcial2.juego;

import parcial2.juego.FreePlay.AdapterElements;
import android.location.Location;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Registrados extends Activity {
	TextView bienv;
	Button register;
	UsuarioClass usuario;
	static UsuarioClass[] conectados;
	UsuarioClass[] isuser;
	HttpHandler h;
	TorneoClass torneo;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registrados);
		actualizar(null);
		
		
	}

	public void actualizar(View view){
		bienv=(TextView) findViewById(R.id.bienvenido);
		h = HttpHandler.getInstance();
		usuario=UserSession.getInstance().usuario;
		register=(Button) findViewById(R.id.registrarme);
		Bundle b =getIntent().getExtras();
		torneo=new TorneoClass(b.getString("torneo"));
		conectados=h.registradosTorneo(usuario.id, torneo.id);
		
		isuser=h.isUser(usuario.id, torneo.id);
		if(conectados[0]!=null)
		{bienv.setText("Registros del Torneo");
		ListView list=(ListView) findViewById(R.id.lista);
		 AdapterElements adapter = new AdapterElements(this);
		list.setAdapter(adapter);}
		else
			bienv.setText("No hay usuarios registrados al torneo");
		
		if(isuser[0]!=null){
			register.setEnabled(false);
		
		}
		else{
			//Toast.makeText(c,"psino: "+torneo.nivel , Toast.LENGTH_SHORT);
			if(Integer.parseInt(torneo.nivel)==Integer.parseInt(usuario.experiencia)/1000){
				register.setEnabled(true);
				//Toast.makeText(c,"si: "+torneo.nivel , Toast.LENGTH_SHORT);
				
			}
			
			else{
				register.setEnabled(false);
			}
		}
		
		
			}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.registrados, menu);
		return true;
	}
	
	class AdapterElements extends ArrayAdapter<Object> {
		Activity c;
		
		public AdapterElements(Activity context) {
    		super(context, R.layout.item, conectados);
    		this.c = context;
		}
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			
			LayoutInflater inflater = c.getLayoutInflater();
			View view = inflater.inflate(R.layout.item, null);
			
			TextView tTitle =(TextView) view.findViewById(R.id.tittle);
			tTitle.setText(conectados[position].nombre);
			
			TextView tdes =(TextView) view.findViewById(R.id.description);
			tdes.setText(conectados[position].experiencia);
			
			Button mas =(Button) view.findViewById(R.id.mas);
			mas.setId(position);
			mas.setOnClickListener(new OnClickListener() {
			      @Override
			   public void onClick(View view) {
			    // TODO Auto-generated method stub
			    		 Intent intento = new Intent(view.getContext(), Perfil.class);
			    	     intento.putExtra("user", conectados[view.getId()].toString());
			    	     
			    	     view.getContext().startActivity(intento);		    	  
			       }
			  });
			
			if(isuser[0]!=null){
		//		register.setEnabled(false);
			Button batallar =(Button) view.findViewById(R.id.batallar);
			batallar.setId(position);
			batallar.setOnClickListener(new OnClickListener() {
			      @Override
			   public void onClick(View view) {
			    // TODO Auto-generated method stub
			    	 
			    	usuario.torneo=Integer.parseInt(torneo.id);
			    	  usuario.enemy=conectados[view.getId()].id;
			    	     view.getContext().startActivity(new Intent(view.getContext(),Batalla.class));		    	  
			       }
			  });
			}
			else{
			//	Toast.makeText(c,"psino: "+torneo.nivel , Toast.LENGTH_SHORT);
				if(Integer.parseInt(torneo.nivel)==Integer.parseInt(usuario.experiencia)/1000){
			//		register.setEnabled(true);
				//	Toast.makeText(c,"si: "+torneo.nivel , Toast.LENGTH_SHORT);
					
				}
				
				else{
					register.setEnabled(false);
				}
			}
			
			return view;
		}
		}

}
