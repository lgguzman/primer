package parcial2.juego;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.andengine.extension.multiplayer.protocol.adt.message.client.ClientMessage;
import org.andengine.extension.multiplayer.protocol.adt.message.server.ServerMessage;


public class Messenger implements Constant{

	
	public static class GFinishClientMessage extends ClientMessage {
		public int mID;
		public int enemy;
		public int messageID;



		public GFinishClientMessage() {

		}

		public GFinishClientMessage(final int pID, final int enemy, int messageID) {
			this.mID = pID;
			this.enemy=enemy;
			this.messageID= messageID;
			

		}

		public void set(final int pID, final int enemy, int messageID) {
			this.mID = pID;
			this.enemy=enemy;
			this.messageID=messageID;


		}


		@Override
		public short getFlag() {
			return FLAG_MESSAGE_CLIENT_GFINISH;
		}

		@Override
		protected void onReadTransmissionData(final DataInputStream pDataInputStream) throws IOException {
			this.mID = pDataInputStream.readInt();
			this.enemy = pDataInputStream.readInt();
			this.messageID=pDataInputStream.readInt();

	
		}

		@Override
		protected void onWriteTransmissionData(final DataOutputStream pDataOutputStream) throws IOException {
			pDataOutputStream.writeInt(this.mID);
			pDataOutputStream.writeInt(this.enemy);
            pDataOutputStream.writeInt(this.messageID);

		}
	}
	
	public static class AddFaceClientMessage extends ClientMessage {
		public int owner;
		public boolean power;
		public int turn;
        public int messageID;
		

		public AddFaceClientMessage() {

		}

		public AddFaceClientMessage(final int owner, final boolean power, final int turn, int messageID) {
			this.owner = owner;
			this.power = power;
			this.turn =turn;
			this.messageID=messageID;

		}

		public void set(final int owner, final boolean power, final int turn, int messageID) {
			this.owner = owner;
			this.power = power;
			this.turn =turn;
			this.messageID=messageID;

		}


		@Override
		public short getFlag() {
			return FLAG_MESSAGE_CLIENT_ADD_FACE;
		}

		@Override
		protected void onReadTransmissionData(final DataInputStream pDataInputStream) throws IOException {
			this.owner = pDataInputStream.readInt();
			this.power = pDataInputStream.readBoolean();
			this.turn = pDataInputStream.readInt();
            this.messageID=pDataInputStream.readInt();

		}

		@Override
		protected void onWriteTransmissionData(final DataOutputStream pDataOutputStream) throws IOException {
			pDataOutputStream.writeInt(this.owner);
			pDataOutputStream.writeBoolean(this.power);
			pDataOutputStream.writeInt(this.turn);
            pDataOutputStream.writeInt(this.messageID);

		}
	}
	
	public static class MoveCardClientMessage extends ClientMessage {
		public int mID;
		public float mX;
		public float mY;
		public int userID;
		public int live;
		public int power;
		public int defend;
		public int attack;
        public boolean active;
        public int cardID;
        public int spritecard;
        public int messageID;

		public MoveCardClientMessage() {

		}

		public MoveCardClientMessage(final int pID, final float pX, final float pY, final int userID,final int live,final int power,final int defend, final int attack,final  int cardID,final  boolean active,final int spritecard, int messageID) {
			this.mID = pID;
			this.mX = pX;
			this.mY = pY;
			this.userID= userID;
			this.live=live;
			this.power=power;
			this.defend=defend;
			this.attack=attack;
			this.cardID=cardID;
			this.active=active;
			this.spritecard=spritecard;
			this.messageID=messageID;

		}

		public void set(final int pID, final float pX, final float pY, final int userID,final int live,final int power,final int defend, final int attack,final  int cardID,final  boolean active,final int spritecard, int messageID) {
			this.mID = pID;
			this.mX = pX;
			this.mY = pY;
			this.userID= userID;
			this.live=live;
			this.power=power;
			this.defend=defend;
			this.attack=attack;
			this.cardID=cardID;
			this.active=active;
			this.spritecard=spritecard;
			this.messageID=messageID;

		}

		@Override
		public short getFlag() {
			return FLAG_MESSAGE_CLIENT_MOVE_CARD;
		}

		@Override
		protected void onReadTransmissionData(final DataInputStream pDataInputStream) throws IOException {
			this.mID = pDataInputStream.readInt();
			this.mX = pDataInputStream.readFloat();
			this.mY = pDataInputStream.readFloat();
			this.userID= pDataInputStream.readInt();
			this.live=pDataInputStream.readInt();
			this.power=pDataInputStream.readInt();
			this.defend=pDataInputStream.readInt();
			this.attack=pDataInputStream.readInt();	
			this.cardID=pDataInputStream.readInt();
			this.active=pDataInputStream.readBoolean();
			this.spritecard= pDataInputStream.readInt();
			this.messageID=pDataInputStream.readInt();

			}

		@Override
		protected void onWriteTransmissionData(final DataOutputStream pDataOutputStream) throws IOException {
			pDataOutputStream.writeInt(this.mID);
			pDataOutputStream.writeFloat(this.mX);
			pDataOutputStream.writeFloat(this.mY);
			pDataOutputStream.writeInt(this.userID);
			pDataOutputStream.writeInt(this.live);
			pDataOutputStream.writeInt(this.power);
			pDataOutputStream.writeInt(this.defend);
			pDataOutputStream.writeInt(this.attack);
			pDataOutputStream.writeInt(this.cardID);
			pDataOutputStream.writeBoolean(this.active);
			pDataOutputStream.writeInt(this.spritecard);
			pDataOutputStream.writeInt(this.messageID);

			
		}
	}
	
	public static class UserClientMessage extends ClientMessage {
		public int mID;
		public int messageID;



		public UserClientMessage() {

		}

		public UserClientMessage(final int pID, int messageID) {
			this.mID = pID;
            this.messageID=messageID;

		}

		public void set(final int pID, int messageID) {
			this.mID = pID;
            this.messageID=messageID;

		}

		@Override
		public short getFlag() {
			return FLAG_MESSAGE_CLIENT_ID_USER;
		}

		@Override
		protected void onReadTransmissionData(final DataInputStream pDataInputStream) throws IOException {
			this.mID = pDataInputStream.readInt();
			this.messageID=pDataInputStream.readInt();

	
		}

		@Override
		protected void onWriteTransmissionData(final DataOutputStream pDataOutputStream) throws IOException {
			pDataOutputStream.writeInt(this.mID);
            pDataOutputStream.writeInt(this.messageID);

		}
	}
	
	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================
	

	public static class ListoServerMessage extends ServerMessage {
		public boolean listo;
		public int messageId;

		public ListoServerMessage() {

		}

		public ListoServerMessage(final boolean listo, int messageId) {
			this.listo = listo;
			this.messageId= messageId;
		}

		public void set(final boolean listo, int messageId) {
			this.listo = listo;
			this.messageId= messageId;
		}

		@Override
		public short getFlag() {
			return FLAG_MESSAGE_SERVER_LISTO;
		}

		@Override
		protected void onReadTransmissionData(final DataInputStream pDataInputStream) throws IOException {
			this.listo = pDataInputStream.readBoolean();
			this.messageId= pDataInputStream.readInt();
		}

		@Override
		protected void onWriteTransmissionData(final DataOutputStream pDataOutputStream) throws IOException {
			pDataOutputStream.writeBoolean(this.listo);
			pDataOutputStream.writeInt(this.messageId);
		}
	}

	
	
	public static class MDestroyServerMessage extends ServerMessage {
		public int mID;
		public int owner;
		public int live;
		public int turn;
		public int DID;
		public int messageID;

		public MDestroyServerMessage() {

		}

		public MDestroyServerMessage(final int pID, final int owner, final int live, final int turn, final int DID,  int messageID) {
			this.mID = pID;
			this.owner =owner;
			this.live = live;
			this.turn= turn;
			this.DID=DID;
			this.messageID= messageID;
		}

		public void set(final int pID, final int owner, final int live, final int turn, final int DID,  int messageID) {
			this.mID = pID;
			this.owner =owner;
			this.live = live;
			this.turn= turn;
			this.DID=DID;
			this.messageID= messageID;
		}
		@Override
		public short getFlag() {
			return FLAG_MESSAGE_SERVER_MDESTROY;
		}

		@Override
		protected void onReadTransmissionData(final DataInputStream pDataInputStream) throws IOException {
			this.mID = pDataInputStream.readInt();
			this.owner = pDataInputStream.readInt();
			this.live = pDataInputStream.readInt();
			this.turn= pDataInputStream.readInt();
			this.DID=pDataInputStream.readInt();
			this.messageID=pDataInputStream.readInt();
			
		}

		@Override
		protected void onWriteTransmissionData(final DataOutputStream pDataOutputStream) throws IOException {
			pDataOutputStream.writeInt(this.mID);
			pDataOutputStream.writeInt(this.owner);
			pDataOutputStream.writeInt(this.live);
			pDataOutputStream.writeInt(this.turn);
			pDataOutputStream.writeInt(this.DID);
			pDataOutputStream.writeInt(this.messageID);
		}
	}

	public static class AddFaceServerMessage extends ServerMessage {
		public int mID;
		public int owner;
		public int live;
		public int turn;
		public int messageID;

		public AddFaceServerMessage() {

		}

		public AddFaceServerMessage(final int pID, final int owner, final int live, final int turn,  int messageID) {
			this.mID = pID;
			this.owner =owner;
			this.live = live;
			this.turn= turn;
			this.messageID= messageID;
		}

		public void set(final int pID, final int owner, final int live, final int turn,  int messageID) {
			this.mID = pID;
			this.owner =owner;
			this.live = live;
			this.turn= turn;
			this.messageID= messageID;
			}

		@Override
		public short getFlag() {
			return FLAG_MESSAGE_SERVER_ADD_FACE;
		}

		@Override
		protected void onReadTransmissionData(final DataInputStream pDataInputStream) throws IOException {
			this.mID = pDataInputStream.readInt();
			this.owner = pDataInputStream.readInt();
			this.live = pDataInputStream.readInt();
			this.turn= pDataInputStream.readInt();
			this.messageID= pDataInputStream.readInt();
		}

		@Override
		protected void onWriteTransmissionData(final DataOutputStream pDataOutputStream) throws IOException {
			pDataOutputStream.writeInt(this.mID);
			pDataOutputStream.writeInt(this.owner);
			pDataOutputStream.writeInt(this.live);
			pDataOutputStream.writeInt(this.turn);
			pDataOutputStream.writeInt(this.messageID);
		}
	}

	public static class MoveCardServerMessage extends ServerMessage {

		public int mID;
		public float mX;
		public float mY;
		public int userID;
		public int live;
		public int power;
		public int defend;
		public int attack;
		public int cardID;
		public boolean active;
	    public int spritecard;
	    public int messageID;

		public MoveCardServerMessage() {

		}

		public MoveCardServerMessage(final int pID, final float pX, final float pY, final int userID,final int live,final int power,final int defend, final int attack,final int cardID,final  boolean active,final int spritecard, int messageID) {
			this.mID = pID;
			this.mX = pX;
			this.mY = pY;
			this.userID= userID;
			this.live=live;
			this.power=power;
			this.defend=defend;
			this.attack=attack;
			this.cardID=cardID;
			this.active=active;
			this.spritecard=spritecard;
			this.messageID=messageID;
		}

		public void set(final int pID, final float pX, final float pY, final int userID,final int live,final int power,final int defend, final int attack, final int cardID,final  boolean active,final  int spritecard, int messageID) {
			this.mID = pID;
			this.mX = pX;
			this.mY = pY;
			this.userID= userID;
			this.live=live;
			this.power=power;
			this.defend=defend;
			this.attack=attack;
			this.cardID=cardID;
			this.active=active;
			this.spritecard=spritecard;
			this.messageID=messageID;
		}

		@Override
		public short getFlag() {
			return FLAG_MESSAGE_SERVER_MOVE_CARD;
		}

		@Override
		protected void onReadTransmissionData(final DataInputStream pDataInputStream) throws IOException {
			this.mID = pDataInputStream.readInt();
			this.mX = pDataInputStream.readFloat();
			this.mY = pDataInputStream.readFloat();
			this.userID= pDataInputStream.readInt();
			this.live=pDataInputStream.readInt();
			this.power=pDataInputStream.readInt();
			this.defend=pDataInputStream.readInt();
			this.attack=pDataInputStream.readInt();	
			this.cardID=pDataInputStream.readInt();
			this.active=pDataInputStream.readBoolean();
			this.spritecard= pDataInputStream.readInt();
			this.messageID=pDataInputStream.readInt();
			}

		@Override
		protected void onWriteTransmissionData(final DataOutputStream pDataOutputStream) throws IOException {
			pDataOutputStream.writeInt(this.mID);
			pDataOutputStream.writeFloat(this.mX);
			pDataOutputStream.writeFloat(this.mY);
			pDataOutputStream.writeInt(this.userID);
			pDataOutputStream.writeInt(this.live);
			pDataOutputStream.writeInt(this.power);
			pDataOutputStream.writeInt(this.defend);
			pDataOutputStream.writeInt(this.attack);
			pDataOutputStream.writeInt(this.cardID);
			pDataOutputStream.writeBoolean(this.active);
			pDataOutputStream.writeInt(this.spritecard);
			pDataOutputStream.writeInt(this.messageID);
			
		}
	}



	public static class UserServerMessage extends ServerMessage {
		public int mID;
		public int enemy;
		public int messageID;


		public UserServerMessage() {

		}

		public UserServerMessage(final int pID, final int enemy,  int messageID) {
			this.mID = pID;
			this.enemy=enemy;
			this.messageID=messageID;

		}

		public void set(final int pID, final int enemy, int messageID) {
			this.mID = pID;
			this.enemy=enemy;
			this.messageID=messageID;

		}

		@Override
		public short getFlag() {
			return FLAG_MESSAGE_SERVER_ID_USER;
		}

		@Override
		protected void onReadTransmissionData(final DataInputStream pDataInputStream) throws IOException {
			this.mID = pDataInputStream.readInt();
			this.enemy = pDataInputStream.readInt();
			this.messageID= pDataInputStream.readInt();
	
		}

		@Override
		protected void onWriteTransmissionData(final DataOutputStream pDataOutputStream) throws IOException {
			pDataOutputStream.writeInt(this.mID);
			pDataOutputStream.writeInt(this.enemy);
			pDataOutputStream.writeInt(this.messageID);

		}
	}
	
	public static class GFinishServerMessage extends ServerMessage {
		public int mID;
		public int enemy;
		public int messageID;


		public GFinishServerMessage() {

		}

		public GFinishServerMessage(final int pID, final int enemy, int messageID) {
			this.mID = pID;
			this.enemy=enemy;
			this.messageID= messageID;

		}

		public void set(final int pID, final int enemy,  int messageID) {
			this.mID = pID;
			this.enemy=enemy;
			this.messageID= messageID;

		}

		@Override
		public short getFlag() {
			return FLAG_MESSAGE_SERVER_GFINISH;
		}

		@Override
		protected void onReadTransmissionData(final DataInputStream pDataInputStream) throws IOException {
			this.mID = pDataInputStream.readInt();
			this.enemy = pDataInputStream.readInt();
			this.messageID= pDataInputStream.readInt();
	
		}

		@Override
		protected void onWriteTransmissionData(final DataOutputStream pDataOutputStream) throws IOException {
			pDataOutputStream.writeInt(this.mID);
			pDataOutputStream.writeInt(this.enemy);
			pDataOutputStream.writeInt(this.messageID);

		}
	}

}
