package parcial2.juego;

import java.util.ArrayList;
import java.util.Scanner;

import parcial2.juego.FreePlay.AdapterElements;
import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;



public class CartaClass {

	int id;
	String nombre;
	String imagen;
	int defenza;
	int experiencia;
	int ataque;
	int vida;
	int poder;
	
	String tipo;
	String elemento;
	
	char cargado;
	public CartaClass(){}
	public CartaClass (String dato){
		if(dato.indexOf("�error")<0 && dato.indexOf("vacio")<0){
		try{
		String [] datos=dato.split(";");
		id=Integer.parseInt(datos[1]);
		nombre=(datos[2]);
		imagen=(datos[3]);
		defenza=Integer.parseInt(datos[4]);
		experiencia=Integer.parseInt(datos[5]);
		ataque=Integer.parseInt(datos[6]);
		vida=Integer.parseInt(datos[7]);
		poder=Integer.parseInt(datos[8]);
		tipo=(datos[9]);
		elemento=(datos[10]);
		if(datos[1]==null || datos[2]==null)
			cargado='n';
			else
			cargado='y';
			}catch(Exception e){
				
				cargado='n';
			}	}
			else{
				
				cargado='e';
			}
	
	}
	
	public String[] split(String s, String r){
		System.out.println("split :"+s);
		String copy=""+s;
		String array[]=new String[10];
		int i;
		int cont=0;
		while((i=copy.indexOf(r))!=-1){
			array[cont]=copy.substring(0,i);
			cont++;
			copy=copy.substring(i+1, copy.length());
		}
		array[cont]=copy;
		
		return array;
	}
	
	public String toString(){
		
		try{StringBuilder sb=new StringBuilder();
		sb.append(";");
		sb.append(this.id);
		sb.append(";");
		sb.append(this.nombre);
		sb.append(";");
		sb.append(this.imagen);
		sb.append(";");
		sb.append(this.defenza);
		sb.append(";");
		sb.append(this.experiencia);
		sb.append(";");
		sb.append(this.ataque);
		sb.append(";");
		sb.append(this.vida);
		sb.append(";");
		sb.append(this.poder);
		sb.append(";");
		sb.append(this.tipo);
		sb.append(";");
		sb.append(this.elemento);
		return sb.toString();
		}catch(Exception e){
			return e.getMessage();
		}
	
		
		
		
	}
	
	public static ArrayList<CartaClass> variasCartas(String dato){

		Scanner s= new Scanner(dato);
		ArrayList<CartaClass> array=new ArrayList<CartaClass>();
		String linea;
		try{
		while(!(linea=s.nextLine()).equals("EOF") && linea!=null && linea.indexOf("�err")<0){
			CartaClass usuario=new CartaClass(linea);
			System.out.println("el elemento cargado"+usuario.cargado);
			System.out.println(usuario.toString());
			if(usuario!=null && usuario.cargado=='y')
			array.add(usuario);
			
		}
		}catch(Exception e){
			System.out.println("error en varias cartas"+e.getMessage());
		}
		return array;
	}
	public static CartaClass[] variasCartasV(String dato){
		CartaClass []u=new CartaClass[1];
		Scanner s= new Scanner(dato);
		ArrayList<CartaClass> array=new ArrayList<CartaClass>();
		String linea;
		try{
		while(!(linea=s.nextLine()).equals("EOF") && linea!=null && linea.indexOf("�err")<0){
			CartaClass usuario=new CartaClass(linea);
			if(usuario!=null && usuario.cargado=='y')
			array.add(usuario);
			
		}
		u=new CartaClass[array.size()];
		int c=0;
		
		for(CartaClass user:array){

			u[c]=user;
					c++;
		}}catch(Exception e){
			System.out.println("error en varias cartas"+e.getMessage());
		}
		return u;
	}
	
	

	
}