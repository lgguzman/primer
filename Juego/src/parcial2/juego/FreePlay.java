package parcial2.juego;



import java.util.ArrayList;
import java.util.List;




import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class FreePlay extends Activity {
TextView bienv;
UsuarioClass usuario;
static UsuarioClass[] conectados;
HttpHandler h;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_free_play);

        actualizar(null);
		
	}

	public void logOut(View view){
		UserSession.destroy();
		h.logout(usuario.id);
		Intent intento = new Intent(this, Login.class);
	     finish();
	     startActivity(intento);
		
	}
	 
	public void perfil(View view){
		 Intent intento = new Intent(this, Perfil.class);
	     intento.putExtra("user", usuario.toString());
	     startActivity(intento);
		
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.free_play, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        Intent intento;
        switch (itemId) {
                case R.id.btTorneos:
                	  intento = new Intent(this, Torneo.class);
                	  finish();
                	  startActivity(intento);
                        break;
                case R.id.btMiscartas:
                	 intento = new Intent(this, Carta.class);
                	 finish();
               	  startActivity(intento);
                        break;
                case R.id.btBuscar:
               	 intento = new Intent(this, Buscar.class);
               	 finish();
              	  startActivity(intento);
                       break;
                case R.id.btLogout:
                	UserSession.destroy();
            		h.logout(usuario.id);
            		intento = new Intent(this, Login.class);
            	     finish();
            	     startActivity(intento);
                	break;
                }
        return super.onOptionsItemSelected(item);
    }
	
	public void actualizar(View view){
		bienv=(TextView) findViewById(R.id.bienvenido);
		h = HttpHandler.getInstance();
		usuario=UserSession.getInstance().usuario;
		conectados=h.conectados(usuario.id);
		
		if(conectados[0]!=null)
		{bienv.setText("Bienvenido: "+usuario.nombre);
		ListView list=(ListView) findViewById(R.id.lista);
		 AdapterElements adapter = new AdapterElements(this);
		list.setAdapter(adapter);}
		else
			bienv.setText("Bienvenido: "+usuario.nombre+"\nNo hay usuarios conectados");
		
		Location l =UbicacionClass.getInstance(this).locationA;
		if(l!=null){
			h.ActualizarU(usuario.id, l.getLatitude()+"", l.getLongitude()+"");
		}
	}
	class AdapterElements extends ArrayAdapter<Object> {
		Activity c;
		
		public AdapterElements(Activity context) {
    		super(context, R.layout.item, conectados);
    		this.c = context;
		}
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			
			LayoutInflater inflater = c.getLayoutInflater();
			View view = inflater.inflate(R.layout.item, null);
			
			TextView tTitle =(TextView) view.findViewById(R.id.tittle);
			tTitle.setText(conectados[position].nombre);
			
			TextView tdes =(TextView) view.findViewById(R.id.description);
			tdes.setText("Experiencia: "+conectados[position].experiencia);
			
			Button mas =(Button) view.findViewById(R.id.mas);
			mas.setId(position);
			mas.setOnClickListener(new OnClickListener() {
			      @Override
			   public void onClick(View view) {
			    // TODO Auto-generated method stub
			    		 Intent intento = new Intent(view.getContext(), Perfil.class);
			    	     intento.putExtra("user", conectados[view.getId()].toString());
			    	     
			    	     view.getContext().startActivity(intento);		    	  
			       }
			  });
			
			
			Button batallar =(Button) view.findViewById(R.id.batallar);
			batallar.setId(position);
			batallar.setOnClickListener(new OnClickListener() {
			      @Override
			   public void onClick(View view) {
			    // TODO Auto-generated method stub
			    	 
			    	   usuario.torneo=-1;
			    	  usuario.enemy=conectados[view.getId()].id;
			    	     view.getContext().startActivity(new Intent(view.getContext(),Batalla.class));		    	  
			       }
			  });
			
			
			return view;
		}
		}
	
	 
	@Override protected void onDestroy() {
		h.logout(usuario.id);
	   super.onDestroy();
	}
}

