package parcial2.juego;


import parcial2.juego.Messenger.UserClientMessage;
import android.location.Location;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class Carta extends Activity {
	UsuarioClass usuario;
	static CartaClass[] conectados;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_carta);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.carta, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        Intent intento;
        switch (itemId) {
            
                case R.id.btTorneos:
                	 intento = new Intent(this, Torneo.class);
                	 finish();
               	  startActivity(intento);
                        break;

                }
        return super.onOptionsItemSelected(item);
    }
	
	public void actualizar(View view){
		
		usuario=UserSession.getInstance().usuario;
		
		if(usuario.cartas.get(0)!=null)
		{
		for(int i=0;i<usuario.cartas.size();i++){
		conectados[i]=usuario.cartas.get(i);
		
		}
		ListView list=(ListView) findViewById(R.id.lista);
		 AdapterElements adapter = new AdapterElements(this);
		list.setAdapter(adapter);}
	}
	

class AdapterElements extends ArrayAdapter<Object> {
	Activity c;
	
	public AdapterElements(Activity context) {
		super(context, R.layout.item, conectados);
		this.c = context;
	}
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		LayoutInflater inflater = c.getLayoutInflater();
		View view = inflater.inflate(R.layout.item, null);
		
		TextView tTitle =(TextView) view.findViewById(R.id.tittle);
		tTitle.setText(conectados[position].nombre);
		
		TextView tdes =(TextView) view.findViewById(R.id.description);
		tdes.setText("Experiencia: "+conectados[position].experiencia);
		
		Button mas =(Button) view.findViewById(R.id.mas);
		mas.setId(position);
		mas.setOnClickListener(new OnClickListener() {
		      @Override
		   public void onClick(View view) {
		    // TODO Auto-generated method stub
		    		 Intent intento = new Intent(view.getContext(), Perfil.class);
		    	     intento.putExtra("user", conectados[view.getId()].toString());
		    	     
		    	     view.getContext().startActivity(intento);		    	  
		       }
		  });
		
		
	
		
		
		return view;
	}
	}


}
